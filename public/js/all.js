$(document).ready(function(){

	/**
	 * Window Ready Functions
	 */
    
    /**
     * Script to initialize Switch element
     */
    $(".confirm-submit").click(function(e) {
        var msg = $(this).data('confirm') || 'Are you sure?';
        var msgText = $(this).data('confirm-text') || '';
        var form  = $(this).data('form');
        e.preventDefault();
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#4456BA",
        }).then(function() {
            $(form).submit();
            swal({
                title: '<i>Loading...</u>',
                type: 'info',
                showCloseButton: false,
                showCancelButton: false,
                showConfirmButton: false,
                html:
                'Please wait as we process your request. <div class="text-center"><div class="glyph-icon icon-spin-4 icon-spin font-size-28" style="display:inline-block"></div></div>',
            });
        });
    });

    $(".confirm-alert").click(function(e) {
        var msg = $(this).data('confirm') || 'Are you sure?';
        var msgText = $(this).data('confirm-text') || '';
        e.preventDefault();
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false,
        }).then(function() {
            window.location = _self.attr("href")
        });
    });
	
    /**
     * Script to initialize Switch element
     */
    $('.input-switch').bootstrapSwitch();

     /**
     * Script to initialize Input Mask
     */
    $(".input-mask").inputmask();

	/**
     * Script to initilize error messages at top of screen
     * @type {[type]}
     */
    var notifBar = $('#notification-show');
    if(notifBar.length > 0){
        var notes = [];

        // notes[''] = '<i class="glyph-icon icon-cog mrg5R"></i>This is a default notification message.';
        // notes['alert'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an alert notification message.';
        // notes['error'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an error notification message.';
        // notes['success'] = '<i class="glyph-icon icon-cog mrg5R"></i>You successfully read this important message.';
        // notes['information'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an information notification message!';
        // notes['notification'] = '<i class="glyph-icon icon-cog mrg5R"></i>This alert needs your attention, but it\'s for demonstration purposes only.';
        // notes['warning'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is a warning for demonstration purposes.';
        // 
        var self = notifBar;
        noty({
            text: self.data('message'),
            type: self.data('type'),
            dismissQueue: true,
            theme: 'agileUI',
            layout: self.data('layout')
        });
    }

    /* Multiselect inputs */
    $(function() { "use strict";
        $(".spinner-input").spinner();

        $('.input-switch').bootstrapSwitch();

        $(".multi-select").multiSelect();
        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    });
    
	/**
	 * Window Load Functions
	 */
	$(window).load(function(){
		$(window).resize();

		/**
		 * Function to call when page loaded hide the preloader
		 * @param  {function} )   { $('#loading').fadeOut( 400, "linear" ); } 
		 * @param  {Count} 300 
		 * @return {Hide Elemenet}     
		 */
		setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
	});

	/**
	 * Window Resize Function
	 */
	$('window').on('resize', function(){

	});
	$(window).resize();
});
/**
 * Includes all quote creation scripts,
 * Author: Virgilio Lalamoro 10/11/2016
 */

(function($) {
	$.script = {

		init : function()
		{
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

			var colFiveIncVal = 53,
				colFiveIncCustomVal = 15,
				colSixIncVal = 80,
				colEightIncVal = 38,
				colNineIncVal = 76,
				col22ValA = 2400,
				col22ValB = 1000,
				col22ValC = 920;

			$this = $.script;

			// Disable all inputs
			$this.addDisablingColumnClass($('.quote-col'));
			$this.removeDisablingColumClass($('.quote-col.col-0'));
			$this.removeDisablingColumClass($('.quote-col.col-1'));
			$this.activateToolsTipster(null);

			// Function for Select with custom fields
			$('body').on('change', '.select-with-custom', function(){
				var val = $(this).find('option:selected').attr('value');

				if(val == 'custom'){
					$(this).siblings('.select-custom-field').removeClass('hidden').focus();
				} else {
					$(this).siblings('.select-custom-field').addClass('hidden');
				}
			});

			// Column One Selection Result
			$('body').on('change', '.doors_frames_selection', function(){
				var val = $(this).find('option:selected').attr('value'),
					upperMost = $(this).closest('.upper-most-row'),
					items = [];
				
				// Custom disabled for some fields
				upperMost.find('.door_height_second').removeAttr('disabled');
				upperMost.find('.door_height_second_custom').removeAttr('disabled');
				upperMost.find('.door_width_second').removeAttr('disabled');
				upperMost.find('.door_width_second_custom').removeAttr('disabled');
				upperMost.find('.door_thick_second').removeAttr('disabled');
				upperMost.find('.door_thick_second_custom').removeAttr('disabled');
				$this.addDisablingColumnClass(upperMost.find('.quote-col'));
				$this.removeDisablingColumClass(upperMost.find('.quote-col.col-0'));
				$this.removeDisablingColumClass(upperMost.find('.quote-col.col-1'));
				$this.activateToolsTipster(upperMost.find('.quote-col.col-1'));

				if(val == 'f-d'){
					$this.removeDisablingColumClass(upperMost.find('.quote-col'));
					$this.activateToolsTipster();
				} else if(val == 'f'){
					$this.removeDisablingColumClass(upperMost.find('.frame-only-col'));
					$this.removeDisablingColumClass(upperMost.find('.both-col'));
					$this.activateToolsTipster(upperMost.find('.frame-only-col'));
				}  else if(val == 'd'){
					$this.removeDisablingColumClass(upperMost.find('.door-only-col'));
					$this.removeDisablingColumClass(upperMost.find('.both-col'));
					$this.activateToolsTipster(upperMost.find('.door-only-col'));
				} else {
					$this.destroyTooltipsterToColumn(upperMost.find('.quote-col'));
				}
			});

			// Column 5 - 10 Door Rebate Selection Result
			$('body').on('change', '.door_rebate_select', function(){
				var val = $(this).find('option:selected').attr('value'),
					upperMost = $(this).closest('.upper-most-row'),
					items = [];
				
				upperMost.find('.col-door-rebate-drop').addClass('col-door-rebate-disable');
				if(val == 'door'){
					upperMost.find('.col-door-choice').removeClass('col-door-rebate-disable');
				} else if(val == 'reveal'){
					upperMost.find('.col-reveal-choice').removeClass('col-door-rebate-disable');
				}
			});

			// Column 5 Selection result will populate the column 31
			$('body').on('change', '.door_height_first', function(){
				$this.autoPopulateThisSelectElementFromThisValue($(this), '.door_height_second');
			});

			// Column 5 Custom field auto populate column 31
			$('body').on('change', '.door_height_first_custom', function(){
				$this.autoPopulateThisSelectCustomElementFromThisValue($(this), '.door_height_second');
			});

			// Column 6 Selection result will populate the column 32
			$('body').on('change', '.door_width_first', function(){
				$this.autoPopulateThisSelectElementFromThisValue($(this), '.door_width_second');
			});

			// Column 6 Custom field auto populate column 32
			$('body').on('change', '.door_width_first_custom', function(){
				$this.autoPopulateThisSelectCustomElementFromThisValue($(this), '.door_width_second');
			});

			// Column 7 Selection result will populate the column 33
			$('body').on('change', '.door_thick_first', function(){
				var val = $(this).val();

				if(val == 'fire_rated_double_door'){
					$('.reveal_rebate_size_drop_custom').val(53)
				}

				$this.autoPopulateThisSelectElementFromThisValue($(this), '.door_thick_second');
			});

			// Column 7 Custom field auto populate column 33
			$('body').on('change', '.door_thick_first_custom', function(){
				$this.autoPopulateThisSelectCustomElementFromThisValue($(this), '.door_thick_second');
			});

			// Column 5 Selection populate column 11
			$('body').on('change', '.door_height_first', function(){
				var val = $(this).find('option:selected').attr('value'),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colFiveIncVal, '.overall_height_display', '.overall_height', upperMost);
			});

			// Column 5 Custom populate column 11
			$('body').on('change', '.door_height_first_custom', function(){
				var val = $(this).val(),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colFiveIncCustomVal, '.overall_height_display', '.overall_height', upperMost);
			});

			// Column 6 Selection populate column 12
			$('body').on('change', '.door_width_first', function(){
				var val = $(this).find('option:selected').attr('value'),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colSixIncVal, '.overall_width_display', '.overall_width', upperMost);
			});

			// Column 6 Custom populate column 12
			$('body').on('change', '.door_width_first_custom', function(){
				var val = $(this).val(),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colSixIncVal, '.overall_width_display', '.overall_width', upperMost);
			});

			// Column 8 Selection populate column 11
			$('body').on('change', '.reveal_height_selection_col_8', function(){
				var val = $(this).find('option:selected').attr('value'),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colEightIncVal, '.overall_height_display', '.overall_height', upperMost);
			});

			// Column 8 Custom populate column 11
			$('body').on('change', '.reveal_height_selection_col_8_custom', function(){
				var val = $(this).val(),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colEightIncVal, '.overall_height_display', '.overall_height', upperMost);
			});

			// Column 9 Selection populate column 12
			$('body').on('change', '.reveal_width_selection_col_9', function(){
				var val = $(this).find('option:selected').attr('value'),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colNineIncVal, '.overall_width_display', '.overall_width', upperMost);
			});

			// Column 9 Custom populate column 12
			$('body').on('change', '.reveal_width_selection_col_9_custom', function(){
				var val = $(this).val(),
					upperMost = $(this).closest('.upper-most-row');

				$this.columnSetDisplayValueAndInputHiddenValue(val, colNineIncVal, '.overall_width_display', '.overall_width', upperMost);
			});

			// Column 10 Reveal Radio Result
			$('body').on('change', '.reveal_radio_option', function(){
				var val = $(this).attr('value'),
					upperMost = $(this).closest('.upper-most-row'),
					items = [];

				upperMost.find('.reveal-drop-result').addClass('hidden');
				if(val == 'door_thick'){
					upperMost.find('.reveal_door_thick').removeClass('hidden');
				} else if(val == 'rebate_size'){
					upperMost.find('.reveal_rebate_size').removeClass('hidden');
				}
			});

			// Column 15 Frame Profile Pic Selection
			$('body').on('click', '.choose_frame_profile_pic', function(){
				var upperMost = $(this).closest('.upper-most-row'),
					line = upperMost.data('line'),
					imgPath = $(this).attr('data-selected-image-path'),
					imgClass = $(this).attr('data-selected-image-class');

				if(imgClass != ''){
					$('.frame_profile_preview').attr('src', imgPath);
				}

				$('#modal-frame-profile-pictures').attr('data-line', line);
				$('#modal-frame-profile-pictures').find('.thumbnail-box').removeClass('selected');
				$('#modal-frame-profile-pictures').find(imgClass).closest('.thumbnail-box').addClass('selected');
			});

			// Modal of choosing images
			$('body').on('click', '.frame_profile_pic_chosen', function(){
				var target = $(this).data('target'),
					imgPath = $(target).attr('src'),
					frameId = $(target).data('id'),
					currLine = $('#modal-frame-profile-pictures').attr('data-line'),
					currElement = $('#row-'+currLine).find('.choose_frame_profile_pic');

				currElement.text('CHANGE IMAGE');
				currElement.attr('data-selected-image-path', imgPath);
				currElement.attr('data-selected-image-class', target);
				$('#row-'+currLine).find('.frame_profile').val(frameId);
				$('.frame_profile_preview').attr('src', imgPath);
				$('#modal-frame-profile-pictures').modal('hide');
			});

			// Column 20 on focus detection generate value
			$('body').on('focus', '.hinge_qty_input', function(){

			});

			// Column 22 on focus detection generate value
			$('body').on('focus', '.hinge_qty_input', function(){
				var upperMost = $(this).closest('.upper-most-row'),
					col11 = parseInt(upperMost.find('.overall_height').attr('value')),
					col12 = parseInt(upperMost.find('.overall_width').attr('value'));

				if(col11 < col22ValA && col12 <= col22ValB){
					$(this).attr('value', 3);
				} else if(col11 >= col22ValA && col12 > col22ValC){
					$(this).attr('value', 4);
				}
			});

			// Column 23 on focus detection generate different values
			$('body').on('focus', '.frame_throat_size', function(){
				var upperMost = $(this).closest('.upper-most-row'),
					col19 = upperMost.find('.wall_finishes option:selected').val(),
					col18 = upperMost.find('.frame_fixing option:selected').val(),
					col17 = upperMost.find('.wall_construction').val(),
					col17Val = 0,
					condArr = ['flat_straps','tube_fixing','top_hat','ties'],
					condArr1 = ['wing_straps'];

				if(col17 != ''){
					col17Arr = col17.split('/');
					for (var i = 0; i < col17Arr.length; i++) {
					    col17Val += parseInt(col17Arr[i]);
					}
				}

				if(col19 == 'flush'){
					$(this).inputmask();
				} else if(col19 == 'pocketed'){
					$(this).inputmask('remove');
					if($.inArray(col18, condArr) != -1){
						$(this).val(col17Val + 4);
					} else if($.inArray(col18, condArr1) != -1){
						$(this).val(col17Val + 6);
					}
				}
			});

			// Column 25 selection result
			$('body').on('change', '.lock_type', function(){
				var upperMost = $(this).closest('.upper-most-row'),
					val = $(this).find('option:selected').val();

				if(val == 'electric'){
					$('.lock_type_electric').removeClass('hidden').focus();
				} else {
					$('.lock_type_electric').addClass('hidden');
				}
			});

			// Copy Rows Action Button
			$('.btn-copy-rows').on('click', function(){
				var valid = $this.checkIfTheresCheckedRow(),
					html = '';
				if(valid){
					$('.quote-rows .rows_check:checked').each(function(){
						var rowCount = $('.quote-rows').length,
							oldRow = $(this).closest('.upper-most-row'),
							row = $(this).closest('.upper-most-row').clone();

						row = $this.updateNameIndexexOfAllInputAndSelectsRowDuplicate(row, oldRow, rowCount);
						$('#quote-table tbody tr:last-child').after(row);
					});

					$('.input-switch').bootstrapSwitch();
				}

				return false;
			});

			// Delete Rows Action Button
			$('.btn-delete-rows').on('click', function(){
				var valid = $this.checkIfTheresCheckedRow();

				if(valid){
					var length = $('.quote-rows .rows_check').length;
					$('.quote-rows .rows_check:checked').each(function(){
						if(length > 1){
							$(this).closest('.upper-most-row').fadeOut(function(){
									$(this).remove();
							});
						}
						length--;
					});
					setTimeout(function(){
						$this.updateNameIndexexInThisRow();
					}, 1000);
				}

				return false;
			});

			// Get Price Action Button For This Row
			$('body').on('click', '.btn-generate-price', function(){
				var counter = $(this).data('counter'),
					route = $(this).data('price-calculate'),
					$this = $(this);
				$.ajax({
					url : route,
					type : 'POST',
					dataType : 'json',
					data : $('#quote-form').serialize() + "&counter=" + counter,
					beforeSend : function(){
						$this.siblings('.price-calculation').text('');
						$this.siblings('.icon-price-preloader').removeClass('hidden');
					},
					success : function(response){
						if(response.success == true){
							$this.siblings('.icon-price-preloader').addClass('hidden');
							if(response.price == 'TBA'){
								$this.siblings('.price-calculation').text(response.price);
							} else {
								$this.siblings('.price-calculation').text(parseFloat(response.price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
							}
						} else {
							swal({
				                title: 'Whoops..',
  								text: response.message,
				            });
						}
					}
				});
			});
		},

		addDisablingColumnClass : function(element)
		{
			element.addClass('col-disable');
		},

		removeDisablingColumClass : function(element)
		{
			element.removeClass('col-disable');
		},

		activateToolsTipster : function(elements = null){
			if(elements == null){
				$('.quote-col').each(function(){
					var message = $(this).data('message');
					if(message != '' && !$(this).hasClass('col-disable')){
						$this.applyTooltipsterToColumn($(this), message);
					}
				});
			} else {
				elements.each(function(){
					var message = $(this).data('message'),
						requireMessage = $(this).data('require-message');
					if(message != '' && !$(this).hasClass('col-disable')){
						$this.applyTooltipsterToColumn($(this), message);
					}
				});
			}
		},

		applyTooltipsterToColumn : function(element, message)
		{
			element.tooltipster({
			   animation: 'fade',
			   delay: 200,
			   theme: 'tooltipster-punk',
			   content: message,
			   maxWidth: 500
			});
		},

		destroyTooltipsterToColumn : function(element)
		{
			element.tooltipster('destroy');
		},

		columnSetDisplayValueAndInputHiddenValue : function(val, IncVal, displayClass, inputClass, upperMost)
		{
			if($.isNumeric(val)){
				var newVal = parseInt(val) + IncVal;
				upperMost.find(displayClass).text(newVal);
				upperMost.find(inputClass).attr('value', newVal);
				
				return false;
			}

			upperMost.find(displayClass).text('0');
			upperMost.find(inputClass).attr('value', 0);
		},

		autoPopulateThisSelectElementFromThisValue : function(initElement, secondElement)
		{
			var val = initElement.find('option:selected').attr('value'),
				upperMost = initElement.closest('.upper-most-row'),
				columnOneVal = upperMost.find('.doors_frames_selection option:selected').val();

			upperMost.find(secondElement).prop('disabled', true);
			upperMost.find(secondElement+'_custom').prop('disabled', true);

			if(columnOneVal == 'f-d'){
				if(val != 'custom'){
					upperMost.find(secondElement+'_custom').addClass('hidden').val();
					upperMost.find(secondElement).find('option[value="'+val+'"]').prop('selected', true);
				}
			}
		},

		autoPopulateThisSelectCustomElementFromThisValue : function(initElement, secondElement)
		{
			var val = initElement.val(),
				upperMost = initElement.closest('.upper-most-row');

			upperMost.find(secondElement).find('option[value="custom"]').prop('selected', true);
			upperMost.find(secondElement+'_custom').val(val).removeClass('hidden');
		},

		checkIfTheresCheckedRow : function()
		{
			var flag = false;
			$('.quote-rows').find('.rows_check').each(function(){
				if($(this).is(':checked')){
					flag = true;
					return false;
				}
			});

			if(flag == false){
				swal(
					'Hold on!',
					'Please check some rows!',
					'error'
				);
			}

			return flag;
		},

		updateNameIndexexOfAllInputAndSelectsRowDuplicate : function(row, oldRow, rowCount)
		{
			var newCount = rowCount + 1;
			// update Select Indexes
			row.find('select').each(function(){
				var oldName = $(this).attr('name').split('['),
					selectedVal = oldRow.find('.'+oldName[0]).find('option:selected').val();

				$(this).attr('name', oldName[0]+'['+newCount+']');
				$(this).find('option[value="'+selectedVal+'"]').prop('selected', true);
			});

			// update Inputs Indexes
			row.find('input').each(function(){
				var oldName = $(this).attr('name').split('[');
				$(this).attr('name', oldName[0]+'['+newCount+']');
			});

			row.find('.btn-generate-price').attr('data-counter', newCount);
			row.attr('id', 'row-'+newCount);
			row.attr('data-line', newCount);
			return row;
		},

		updateNameIndexexInThisRow : function()
		{
			var i = 1;
			$('.quote-rows').each(function(){
				$(this).find('select').each(function(){
					var oldName = $(this).attr('name').split('[');
					$(this).attr('name', oldName[0]+'['+i+']');
				});

				// update Inputs Indexes
				$(this).find('input').each(function(){
					var oldName = $(this).attr('name').split('[');
					$(this).attr('name', oldName[0]+'['+i+']');
				});

				$(this).find('.btn-generate-price').attr('data-counter', i);
				$(this).attr('id', 'row-'+i);
				$(this).attr('data-line', i);
				i++;
			});
		}
	};

	$.script.init();
})(jQuery);
//# sourceMappingURL=all.js.map
