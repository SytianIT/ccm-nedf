<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCombinationPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combination_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('col_11');
            $table->integer('col_12');
            $table->string('col_13');
            $table->string('col_14');
            $table->integer('col_23');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('combination_pricing');
    }
}
