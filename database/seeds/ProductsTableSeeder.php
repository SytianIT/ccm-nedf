<?php

use App\Products\PriceList;
use App\Products\Product;
use App\Products\ProductPrice;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        PriceList::truncate();
        ProductPrice::truncate();

        $doorHeight = new Product;
        $doorHeight->name = 'door_height';
        $doorHeight->title = 'Door Height';
        $doorHeight->save();

        $dhItem1 = new Product;
        $dhItem1->name = '2040';
        $dhItem1->title = '2040';
        $dhItem1->parent_id = $doorHeight->id;
        $dhItem1->save();

        $dhItem2 = new Product;
        $dhItem2->name = '2340';
        $dhItem2->title = '2340';
        $dhItem2->parent_id = $doorHeight->id;
        $dhItem2->save();

        $dhItem3 = new Product;
        $dhItem3->name = '2640';
        $dhItem3->title = '2640';
        $dhItem3->parent_id = $doorHeight->id;
        $dhItem3->save();

    	$doorWidth = new Product;
        $doorWidth->name = 'door_width';
        $doorWidth->title = 'Door Width';
        $doorWidth->save();

        $dwItem1 = new Product;
        $dwItem1->name = '820';
        $dwItem1->title = '820';
        $dwItem1->parent_id = $doorWidth->id;
        $dwItem1->save();        

        $dwItem2 = new Product;
        $dwItem2->name = '870';
        $dwItem2->title = '870';
        $dwItem2->parent_id = $doorWidth->id;
        $dwItem2->save();   

        $dwItem3 = new Product;
        $dwItem3->name = '920';
        $dwItem3->title = '920';
        $dwItem3->parent_id = $doorWidth->id;
        $dwItem3->save(); 

        $dwItem4 = new Product;
        $dwItem4->name = '970';
        $dwItem4->title = '970';
        $dwItem4->parent_id = $doorWidth->id;
        $dwItem4->save(); 

        $dwItem5 = new Product;
        $dwItem5->name = '1020';
        $dwItem5->title = '1020';
        $dwItem5->parent_id = $doorWidth->id;
        $dwItem5->save(); 

        $dwItem6 = new Product;
        $dwItem6->name = '1120';
        $dwItem6->title = '1120';
        $dwItem6->parent_id = $doorWidth->id;
        $dwItem6->save(); 

        $dwItem7 = new Product;
        $dwItem7->name = '1200';
        $dwItem7->title = '1200';
        $dwItem7->parent_id = $doorWidth->id;
        $dwItem7->save(); 

        $dwItem8 = new Product;
        $dwItem8->name = 'dd';
        $dwItem8->title = 'DD (Double Door)';
        $dwItem8->parent_id = $doorWidth->id;
        $dwItem8->save(); 

        $doorThick = new Product;
        $doorThick->name = 'door_thick';
        $doorThick->title = 'Door Thick';
        $doorThick->save();

        $dtItem1 = new Product;
        $dtItem1->name = '35_sc';
        $dtItem1->title = '35 SC';
        $dtItem1->parent_id = $doorThick->id;
        $dtItem1->save();         

        $dtItem2 = new Product;
        $dtItem2->name = '40_sc';
        $dtItem2->title = '40 SC';
        $dtItem2->parent_id = $doorThick->id;
        $dtItem2->save(); 

        $dtItem3 = new Product;
        $dtItem3->name = '45_sc';
        $dtItem3->title = '45 SC';
        $dtItem3->parent_id = $doorThick->id;
        $dtItem3->save();    

        $dtItem4 = new Product;
        $dtItem4->name = '37_fd';
        $dtItem4->title = '37 fd';
        $dtItem4->parent_id = $doorThick->id;
        $dtItem4->save();    

        $dtItem5 = new Product;
        $dtItem5->name = '47_fd';
        $dtItem5->title = '47 fd';
        $dtItem5->parent_id = $doorThick->id;
        $dtItem5->save();   

        $dtItem5 = new Product;
        $dtItem5->name = 'fire_rated_double_door';
        $dtItem5->title = 'Fire Rated Double Door';
        $dtItem5->parent_id = $doorThick->id;
        $dtItem5->save();  

        $revealHeight = new Product;
        $revealHeight->name = 'reveal_height';
        $revealHeight->title = 'Frame Reveal Height';
        $revealHeight->save();

        $rhItem1 = new Product;
        $rhItem1->name = '2060';
        $rhItem1->title = '2630';
        $rhItem1->parent_id = $revealHeight->id;
        $rhItem1->save(); 

        $rhItem2 = new Product;
        $rhItem2->name = '2360';
        $rhItem2->title = '2360';
        $rhItem2->parent_id = $revealHeight->id;
        $rhItem2->save(); 

        $rhItem3 = new Product;
        $rhItem3->name = '2660';
        $rhItem3->title = '2660';
        $rhItem3->parent_id = $revealHeight->id;
        $rhItem3->save(); 

        $rhItem4 = new Product;
        $rhItem4->name = '2960';
        $rhItem4->title = '2960';
        $rhItem4->parent_id = $revealHeight->id;
        $rhItem4->save(); 

        $revealWidth = new Product;
        $revealWidth->name = 'reveal_width';
        $revealWidth->title = 'Frame Reveal Width';
        $revealWidth->save();

        $rwItem1 = new Product;
        $rwItem1->name = '824';
        $rwItem1->title = '824';
        $rwItem1->parent_id = $revealWidth->id;
        $rwItem1->save(); 

        $rwItem2 = new Product;
        $rwItem2->name = '874';
        $rwItem2->title = '874';
        $rwItem2->parent_id = $revealWidth->id;
        $rwItem2->save(); 

        $rwItem3 = new Product;
        $rwItem3->name = '924';
        $rwItem3->title = '924';
        $rwItem3->parent_id = $revealWidth->id;
        $rwItem3->save(); 

        $rebateSize = new Product;
        $rebateSize->name = 'rebate_size';
        $rebateSize->title = 'Rebate Size';
        $rebateSize->save();

        $rsItem1 = new Product;
        $rsItem1->name = 'nedf_standard';
        $rsItem1->title = 'NEDF Standard';
        $rsItem1->parent_id = $rebateSize->id;
        $rsItem1->save();

        $handForm = new Product;
        $handForm->name = 'hand_form';
        $handForm->title = 'Hand LH / RH';
        $handForm->save();

        $hfItem1 = new Product;
        $hfItem1->name = 'lh';
        $hfItem1->title = 'LH';
        $hfItem1->parent_id = $handForm->id;
        $hfItem1->save();

        $hfItem2 = new Product;
        $hfItem2->name = 'rh';
        $hfItem2->title = 'RH';
        $hfItem2->parent_id = $handForm->id;
        $hfItem2->save();

        $hfItem3 = new Product;
        $hfItem3->name = 'dd';
        $hfItem3->title = 'DD';
        $hfItem3->parent_id = $handForm->id;
        $hfItem3->save();

        $hfItem4 = new Product;
        $hfItem4->name = 'fire_rated_double_door_rebate';
        $hfItem4->title = 'Fire Rated Double Door';
        $hfItem4->parent_id = $handForm->id;
        $hfItem4->save();

        $gaugeMaterial = new Product;
        $gaugeMaterial->name = 'gauge_material';
        $gaugeMaterial->title = 'Gauge & Material';
        $gaugeMaterial->save();

        $gmItem1 = new Product;
        $gmItem1->name = '1_1_z';
        $gmItem1->title = '1.1 z';
        $gmItem1->parent_id = $gaugeMaterial->id;
        $gmItem1->save();

        $gmItem2 = new Product;
        $gmItem2->name = '1_4_z';
        $gmItem2->title = '1.4 z';
        $gmItem2->parent_id = $gaugeMaterial->id;
        $gmItem2->save();

        $gmItem3 = new Product;
        $gmItem3->name = '2_5_z';
        $gmItem3->title = '2.5 z';
        $gmItem3->parent_id = $gaugeMaterial->id;
        $gmItem3->save();

        $gmItem4 = new Product;
        $gmItem4->name = '1_1_gal';
        $gmItem4->title = '1.1 gal';
        $gmItem4->parent_id = $gaugeMaterial->id;
        $gmItem4->save();

        $gmItem5 = new Product;
        $gmItem5->name = '1_4_gal';
        $gmItem5->title = '1.4 gal';
        $gmItem5->parent_id = $gaugeMaterial->id;
        $gmItem5->save();

        $gmItem6 = new Product;
        $gmItem6->name = '2_5_gal';
        $gmItem6->title = '2.5 gal';
        $gmItem6->parent_id = $gaugeMaterial->id;
        $gmItem6->save();

        $gmItem7 = new Product;
        $gmItem7->name = '1_2_stainless';
        $gmItem7->title = '1.2 stainless';
        $gmItem7->parent_id = $gaugeMaterial->id;
        $gmItem7->save();

        $gmItem8 = new Product;
        $gmItem8->name = '1_5_stainless';
        $gmItem8->title = '1.5 stainless';
        $gmItem8->parent_id = $gaugeMaterial->id;
        $gmItem8->save();

        $gmItem9 = new Product;
        $gmItem9->name = '2_5_stainless';
        $gmItem9->title = '2.5 stainless';
        $gmItem9->parent_id = $gaugeMaterial->id;
        $gmItem9->save();

        $stormMould = new Product;
        $stormMould->name = 'mould';
        $stormMould->title = 'Mould';
        $stormMould->save();

        $stormMouldYes = new Product;
        $stormMouldYes->name = 'mould_yes';
        $stormMouldYes->title = 'Yes';
        $stormMouldYes->parent_id = $stormMould->id;
        $stormMouldYes->save();

        $stormMouldNo = new Product;
        $stormMouldNo->name = 'mould_no';
        $stormMouldNo->title = 'No';
        $stormMouldNo->parent_id = $stormMould->id;
        $stormMouldNo->save();

        $wallConstruction = new Product;
        $wallConstruction->name = 'wall_construction';
        $wallConstruction->title = 'Wall Construction';
        $wallConstruction->display = false;
        $wallConstruction->save();

        $frameFixing = new Product;
        $frameFixing->name = 'frame_fixing';
        $frameFixing->title = 'Frame Fixing';
        $frameFixing->save();

        $ffItem1 = new Product;
        $ffItem1->name = 'stud_clips';
        $ffItem1->title = 'Stud Clips';
        $ffItem1->parent_id = $frameFixing->id;
        $ffItem1->save();

        $ffItem2 = new Product;
        $ffItem2->name = 'wing_straps';
        $ffItem2->title = 'Wing Straps';
        $ffItem2->parent_id = $frameFixing->id;
        $ffItem2->save();

        $ffItem2 = new Product;
        $ffItem2->name = 'flat_straps';
        $ffItem2->title = 'Flat Straps';
        $ffItem2->parent_id = $frameFixing->id;
        $ffItem2->save();

        $ffItem3 = new Product;
        $ffItem3->name = 'tube_fixing';
        $ffItem3->title = 'Tube Fixing';
        $ffItem3->parent_id = $frameFixing->id;
        $ffItem3->save();

        $ffItem4 = new Product;
        $ffItem4->name = 'top_hat';
        $ffItem4->title = 'Top Hat';
        $ffItem4->parent_id = $frameFixing->id;
        $ffItem4->save();

        $ffItem5 = new Product;
        $ffItem5->name = 'ties';
        $ffItem5->title = 'Ties';
        $ffItem5->parent_id = $frameFixing->id;
        $ffItem5->save();

        $ffItem6 = new Product;
        $ffItem6->name = 'nedf_make';
        $ffItem6->title = 'NEDF to make decision on behalf of customer';
        $ffItem6->parent_id = $frameFixing->id;
        $ffItem6->save();

        $wallFinishes = new Product;
        $wallFinishes->name = 'wall_finishes';
        $wallFinishes->title = 'Wall Finishes';
        $wallFinishes->save();

        $wfItem1 = new Product;
        $wfItem1->name = 'flush';
        $wfItem1->title = 'Flush to Wall';
        $wfItem1->parent_id = $wallFinishes->id;
        $wfItem1->save();

        $wfItem2 = new Product;
        $wfItem2->name = 'pocketed';
        $wfItem2->title = 'Pocketed';
        $wfItem2->parent_id = $wallFinishes->id;
        $wfItem2->save();

        $hingeType = new Product;
        $hingeType->name = 'hinge_type';
        $hingeType->title = 'Hinge Type';
        $hingeType->save();

        $htItem1 = new Product;
        $htItem1->name = 'lift_off';
        $htItem1->title = 'Lift Off';
        $htItem1->parent_id = $hingeType->id;
        $htItem1->save();

        $htItem2 = new Product;
        $htItem2->name = 'butt';
        $htItem2->title = 'Butt';
        $htItem2->parent_id = $hingeType->id;
        $htItem2->save();

        $htItem3 = new Product;
        $htItem3->name = 'fast_fix';
        $htItem3->title = 'Fast Fix';
        $htItem3->parent_id = $hingeType->id;
        $htItem3->save();

        $htItem4 = new Product;
        $htItem4->name = 'continuous_piano';
        $htItem4->title = 'Continuous Piano';
        $htItem4->parent_id = $hingeType->id;
        $htItem4->save();

        $htItem4 = new Product;
        $htItem4->name = 'pivot_pin';
        $htItem4->title = 'Pivot Pin';
        $htItem4->parent_id = $hingeType->id;
        $htItem4->save();

        $hingeSize = new Product;
        $hingeSize->name = 'hinge_size';
        $hingeSize->title = 'Hinge Size';
        $hingeSize->save();

        $hsItem1 = new Product;
        $hsItem1->name = '100_75_2_5_lift_off_butt';
        $hsItem1->title = '100 * 75 * 2.5 Lift Off / Butt';
        $hsItem1->parent_id = $hingeSize->id;
        $hsItem1->save();

        $hsItem2 = new Product;
        $hsItem2->name = '100_75_3_2_butt';
        $hsItem2->title = '100 * 75 * 3.2 Butt';
        $hsItem2->parent_id = $hingeSize->id;
        $hsItem2->save();

        $hsItem3 = new Product;
        $hsItem3->name = '100_100_2_5_butt';
        $hsItem3->title = '100 * 100 * 2.5 Butt';
        $hsItem3->parent_id = $hingeSize->id;
        $hsItem3->save();

        $hsItem4 = new Product;
        $hsItem4->name = '100_100_3_2_butt';
        $hsItem4->title = '100 * 100 * 3.2 Butt';
        $hsItem4->parent_id = $hingeSize->id;
        $hsItem4->save();

        $hingeQty = new Product;
        $hingeQty->name = 'hinge_quantity';
        $hingeQty->title = 'Hinge Quantity';
        $hingeQty->display = false;
        $hingeQty->save();

        $frameThroat = new Product;
        $frameThroat->name = 'frame_throat';
        $frameThroat->title = 'Frame Throat Size';
        $frameThroat->display = false;
        $frameThroat->save();

        $hingeFixing = new Product;
        $hingeFixing->name = 'hinge_fixing';
        $hingeFixing->title = 'Hinge Fixing';
        $hingeFixing->save();

        $hfItem1 = new Product;
        $hfItem1->name = 'weld';
        $hfItem1->title = 'Weld';
        $hfItem1->parent_id = $hingeFixing->id;
        $hfItem1->save();

        $hfItem2 = new Product;
        $hfItem2->name = 'plate';
        $hfItem2->title = 'Plate';
        $hfItem2->parent_id = $hingeFixing->id;
        $hfItem2->save();

        $lockType = new Product;
        $lockType->name = 'lock_type';
        $lockType->title = 'Lock Type';
        $lockType->save();

        $ltItem1 = new Product;
        $ltItem1->name = 'universal';
        $ltItem1->title = 'Universal';
        $ltItem1->parent_id = $lockType->id;
        $ltItem1->save();

        $ltItem2 = new Product;
        $ltItem2->name = 'electric';
        $ltItem2->title = 'Electric';
        $ltItem2->parent_id = $lockType->id;
        $ltItem2->save();

        $ltItem3 = new Product;
        $ltItem3->name = 'deadlock';
        $ltItem3->title = 'Deadlock';
        $ltItem3->parent_id = $lockType->id;
        $ltItem3->save();

        $ltItem4 = new Product;
        $ltItem4->name = 'rollerbolt';
        $ltItem4->title = 'RollerBolt';
        $ltItem4->parent_id = $lockType->id;
        $ltItem4->save();

        $ltItem5 = new Product;
        $ltItem5->name = 'nightlatch';
        $ltItem5->title = 'Nightlatch';
        $ltItem5->parent_id = $lockType->id;
        $ltItem5->save();

        $lockHeight = new Product;
        $lockHeight->name = 'lock_height_striker';
        $lockHeight->title = 'Lock Height / Striker';
        $lockHeight->save();

        $lhsItem1 = new Product;
        $lhsItem1->name = 'standard_1030';
        $lhsItem1->title = 'Standard 1030';
        $lhsItem1->parent_id = $lockHeight->id;
        $lhsItem1->save();

        $lhsItem2 = new Product;
        $lhsItem2->name = 'ne_latch';
        $lhsItem2->title = 'NE Latch';
        $lhsItem2->parent_id = $lockHeight->id;
        $lhsItem2->save();

        $lhsItem3 = new Product;
        $lhsItem3->name = 'top_latch_turn';
        $lhsItem3->title = 'Top Latch & Turn';
        $lhsItem3->parent_id = $lockHeight->id;
        $lhsItem3->save();

        $lhsItem4 = new Product;
        $lhsItem4->name = 'roller_bold';
        $lhsItem4->title = 'Roller Bolt';
        $lhsItem4->parent_id = $lockHeight->id;
        $lhsItem4->save();

        $lhsItem5 = new Product;
        $lhsItem5->name = 'v_lock';
        $lhsItem5->title = 'V-Lock';
        $lhsItem5->parent_id = $lockHeight->id;
        $lhsItem5->save();

        $lhsItem6 = new Product;
        $lhsItem6->name = 'fire_rated_specifics';
        $lhsItem6->title = 'Fire Rated Specifics';
        $lhsItem6->parent_id = $lockHeight->id;
        $lhsItem6->save();

        $closer = new Product;
        $closer->name = 'closer';
        $closer->title = 'Closer';
        $closer->save();

        $clItem1 = new Product;
        $clItem1->name = 'closer_yes';
        $clItem1->title = 'Yes';
        $clItem1->parent_id = $closer->id;
        $clItem1->save();

        $clItem2 = new Product;
        $clItem2->name = 'closer_no';
        $clItem2->title = 'No';
        $clItem2->parent_id = $closer->id;
        $clItem2->save();

        $welded = new Product;
        $welded->name = 'kd_welded';
        $welded->title = 'K.D / Welded';
        $welded->save();

        $wItem1 = new Product;
        $wItem1->name = 'knock_down';
        $wItem1->title = 'Knock Down';
        $wItem1->parent_id = $welded->id;
        $wItem1->save();

        $wItem2 = new Product;
        $wItem2->name = 'welded';
        $wItem2->title = 'Welded';
        $wItem2->parent_id = $welded->id;
        $wItem2->save();

        $primingPowder = new Product;
        $primingPowder->name = 'priming_powder_coating';
        $primingPowder->title = 'Priming / Powder Coating';
        $primingPowder->save();

        $primingPowderYes = new Product;
        $primingPowderYes->name = 'priming_powder_coating_yes';
        $primingPowderYes->title = 'Yes';
        $primingPowderYes->parent_id = $primingPowder->id;
        $primingPowderYes->save();

        $primingPowderNo = new Product;
        $primingPowderNo->name = 'priming_powder_coating_no';
        $primingPowderNo->title = 'No';
        $primingPowderNo->parent_id = $primingPowder->id;
        $primingPowderNo->save();

        $primingPowderCustom = new Product;
        $primingPowderCustom->name = 'powdercoating';
        $primingPowderCustom->title = 'Powdercoating';
        $primingPowderCustom->parent_id = $primingPowder->id;
        $primingPowderCustom->save();

        $doorType = new Product;
        $doorType->name = 'door_type';
        $doorType->title = 'Door Type';
        $doorType->save();

        $dtyItem1 = new Product;
        $dtyItem1->name = 'hollow_core';
        $dtyItem1->title = 'Hollow Core';
        $dtyItem1->parent_id = $doorType->id;
        $dtyItem1->save();

        $dtyItem2 = new Product;
        $dtyItem2->name = 'solid_core';
        $dtyItem2->title = 'Solid Core';
        $dtyItem2->parent_id = $doorType->id;
        $dtyItem2->save();

        $dtyItem3 = new Product;
        $dtyItem3->name = 'block_core';
        $dtyItem3->title = 'Block Core';
        $dtyItem3->parent_id = $doorType->id;
        $dtyItem3->save();

        $dtyItem4 = new Product;
        $dtyItem4->name = 'semi_solid';
        $dtyItem4->title = 'Semi Solid';
        $dtyItem4->parent_id = $doorType->id;
        $dtyItem4->save();

        $dtyItem5 = new Product;
        $dtyItem5->name = '1hr_fire';
        $dtyItem5->title = '1hr Fire';
        $dtyItem5->parent_id = $doorType->id;
        $dtyItem5->save();

        $dtyItem6 = new Product;
        $dtyItem6->name = '2hr_fire';
        $dtyItem6->title = '2hr Fire';
        $dtyItem6->parent_id = $doorType->id;
        $dtyItem6->save();

        $dtyItem7 = new Product;
        $dtyItem7->name = '3hr_fire';
        $dtyItem7->title = '3hr Fire';
        $dtyItem7->parent_id = $doorType->id;
        $dtyItem7->save();

        $dtyItem8 = new Product;
        $dtyItem8->name = '4hr_fire';
        $dtyItem8->title = '4hr Fire';
        $dtyItem8->parent_id = $doorType->id;
        $dtyItem8->save();

        // $doorHeightSecond = new Product;
        // $doorHeightSecond->name = 'door_height_second';
        // $doorHeightSecond->title = 'Door Height';
        // $doorHeightSecond->display = false;
        // $doorHeightSecond->save();

        // $doorWidthSecond = new Product;
        // $doorWidthSecond->name = 'door_width_second';
        // $doorWidthSecond->title = 'Door Width';
        // $doorWidthSecond->display = false;
        // $doorWidthSecond->save();

        // $doorThick = new Product;
        // $doorThick->name = 'door_thick_second';
        // $doorThick->title = 'Door Thick';
        // $doorThick->display = false;
        // $doorThick->save();

        $wholesale = new PriceList;
        $wholesale->name = PriceList::WHOLESALE;
        $wholesale->title = 'Wholesale';
        $wholesale->save();

        $tradeDiscount = new PriceList;
        $tradeDiscount->name = PriceList::TRADE_DISCOUNT;
        $tradeDiscount->title = 'Trade Discount';
        $tradeDiscount->save();

        $trade = new PriceList;
        $trade->name = PriceList::TRADE;
        $trade->title = 'Trade';
        $trade->save();

        $retail = new PriceList;
        $retail->name = PriceList::RETAIL;
        $retail->title = 'Retail';
        $retail->save();
    }
}
