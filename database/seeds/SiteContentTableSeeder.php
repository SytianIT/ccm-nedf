<?php

use App\SiteContent;
use Illuminate\Database\Seeder;

class SiteContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteContent::truncate();

        $content = new SiteContent;
        $content->email = 'email@email.com';
        $content->site_title = 'North Eastern Door Frames';
        $content->is_active = true;
        $content->save();
    }
}
