<?php

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::truncate();
    	Role::truncate();
    	Permission::truncate();
    	DB::table('user_roles')->truncate();
        DB::table('role_permissions')->truncate();

        /**
         * Creating User
         */
        $admin = new User;
        $admin->firstname = "Administrator";
        $admin->username = "admin";
        $admin->email = "admin@email.com";
        $admin->password = Hash::make('admin123');
        $admin->save();

        $member = new User;
        $member->firstname = "Member";
        $member->username = "member";
        $member->email = "member@email.com";
        $member->password = Hash::make('member123');
        $member->price_list_id = 1;
        $member->save();

        /**
         * Creating Roles
         */
        $adminRole = new Role;
        $adminRole->name = "administrator";
        $adminRole->title = "Administrator";
        $adminRole->save();

        $memberRole = new Role;
        $memberRole->name = "member";
        $memberRole->title = "Member";
        $memberRole->save();

        /**
         * Creating Permissions
         */
        $memberPerm = new Permission;
        $memberPerm->name = "member";
        $memberPerm->title = "Member";
        $memberPerm->save();

        $adminPerm = new Permission;
        $adminPerm->name = "admin";
        $adminPerm->title = "Admin";
        $adminPerm->save();

        $siteSettings = new Permission;
        $siteSettings->name = "access_site_settings";
        $siteSettings->title = "Access Site Settings";
        $siteSettings->save();

        $viewLogs = new Permission;
        $viewLogs->name = "view_logs";
        $viewLogs->title = "View Audit Logs";
        $viewLogs->save();

        $viewRoles = new Permission;
        $viewRoles->name = "view_roles";
        $viewRoles->title = "View Roles";
        $viewRoles->save();

        $createRole = new Permission;
        $createRole->name = "create_role";
        $createRole->title = "Create Role";
        $createRole->save();

        $updateRole = new Permission;
        $updateRole->name = "update_role";
        $updateRole->title = "Update Role";
        $updateRole->save();

        $deleteRole = new Permission;
        $deleteRole->name = "delete_role";
        $deleteRole->title = "Delete Role";
        $deleteRole->save();

        $viewAdmins = new Permission;
        $viewAdmins->name = "view_admins";
        $viewAdmins->title = "View Admins";
        $viewAdmins->save();

        $createAdmins = new Permission;
        $createAdmins->name = "create_admins";
        $createAdmins->title = "Create Admins";
        $createAdmins->save();

        $updateAdmins = new Permission;
        $updateAdmins->name = "update_admins";
        $updateAdmins->title = "Update Admins";
        $updateAdmins->save();

        $deleteAdmins = new Permission;
        $deleteAdmins->name = "delete_admins";
        $deleteAdmins->title = "Delete Admins";
        $deleteAdmins->save();

        $viewMembers = new Permission;
        $viewMembers->name = "view_members";
        $viewMembers->title = "View Members";
        $viewMembers->save();

        $createMember = new Permission;
        $createMember->name = "create_member";
        $createMember->title = "Create Member";
        $createMember->save();

        $updateMember = new Permission;
        $updateMember->name = "update_member";
        $updateMember->title = "Update Member";
        $updateMember->save();

        $deleteMember = new Permission;
        $deleteMember->name = "delete_member";
        $deleteMember->title = "Delete Member";
        $deleteMember->save();

        $viewBasePriceList = new Permission;
        $viewBasePriceList->name = "view_base_pricelists";
        $viewBasePriceList->title = "View Base Price Lists";
        $viewBasePriceList->save();

        $editBasePriceList = new Permission;
        $editBasePriceList->name = "edit_base_pricelists";
        $editBasePriceList->title = "Edit Base Price Lists";
        $editBasePriceList->save();

        // $allocatePriceList = new Permission;
        // $allocatePriceList->name = "allocate_pricelist";
        // $allocatePriceList->title = "Allocate Price List";
        // $allocatePriceList->save();

        $viewQuotes = new Permission;
        $viewQuotes->name = "view_quotes";
        $viewQuotes->title = "View Quotes";
        $viewQuotes->save();

        $updateQuote = new Permission;
        $updateQuote->name = "update_quote";
        $updateQuote->title = "Update Quote";
        $updateQuote->save();

        $deleteQuote = new Permission;
        $deleteQuote->name = "delete_quote";
        $deleteQuote->title = "Delete Quote";
        $deleteQuote->save();

        /**
         * Relating different roles and permission for users
         */
        $admin->roles()->attach($adminRole);
        $member->roles()->attach($memberRole);

        $adminRole->permissions()->attach($adminPerm);
        $adminRole->permissions()->attach($siteSettings);
        $adminRole->permissions()->attach($viewLogs);
        $adminRole->permissions()->attach($viewRoles);
        $adminRole->permissions()->attach($createRole);
        $adminRole->permissions()->attach($updateRole);
        $adminRole->permissions()->attach($deleteRole);
        $adminRole->permissions()->attach($viewAdmins);
        $adminRole->permissions()->attach($createAdmins);
        $adminRole->permissions()->attach($updateAdmins);
        $adminRole->permissions()->attach($deleteAdmins);
        $adminRole->permissions()->attach($viewMembers);
        $adminRole->permissions()->attach($createMember);
        $adminRole->permissions()->attach($updateMember);
        $adminRole->permissions()->attach($deleteMember);
        $adminRole->permissions()->attach($viewBasePriceList);
        $adminRole->permissions()->attach($editBasePriceList);
        // $adminRole->permissions()->attach($allocatePriceList);
        $adminRole->permissions()->attach($viewQuotes);
        $adminRole->permissions()->attach($updateQuote);
        $adminRole->permissions()->attach($deleteQuote);

        $memberRole->permissions()->attach($memberPerm);
    }
}
