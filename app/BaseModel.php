<?php

namespace App;

use App\AuditLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
    /**
	 * Method call to insert log for any database entry
	 * @param  String $description
	 * @param  Object $syion       
	 * @param  Object $user        
	 * @return Boolean              
	 */
    public function insertAuditLog($description, $user = null)
	{
		$user = Auth::user() ? Auth::user() : $user; 
		$log = new AuditLog;
		$log->description = $description;
		$log->user()->associate($user);
		$log->auditable()->associate($this);
		$log->save();

		return true;
	}
}
