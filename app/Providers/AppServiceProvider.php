<?php

namespace App\Providers;

use App\Products\FrameProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * General Application Provider
         * @var view
         */
        View::composer([
            'layouts.app',
            'account.*'
            ], function($view)
        {
            $user = Auth::user();
            
            $view->with('authUser', $user ? $user : null);
        });

        /**
         * Frame Profile Varaible Provider
         */
        View::composer([
            'layouts.inc.modal-quote-frame-profile'
            ], function($view)
        {
            $frameProfiles = FrameProfile::orderBy('created_at', 'DESC')->get();

            $view->with('frameProfiles', $frameProfiles->count() > 0 ? $frameProfiles : 0);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
