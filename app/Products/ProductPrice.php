<?php

namespace App\Products;

use App\Products\Combination;
use App\Products\PriceList;
use App\Products\Product;
use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $table = 'product_price_lists';

    public function product()
    {
    	return $this->belongsTo(Product::class, 'product_id');
    }

    public function priceList()
    {
    	return $this->belongsTo(PriceList::class, 'price_list_id');
    }

    public function combination()
    {
        return $this->belongsTo(Combination::class, 'combination_id');   
    }

    public function member()
    {
        return $this->hasMany(User::class, 'user_id');
    }

    /**
     * Getters
     */
    
}
