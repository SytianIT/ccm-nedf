<?php

namespace App\Products;

use App\Products\PriceList;
use App\Products\Product;
use App\Products\ProductPrice;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    /**
     * Update the product price in third table product_price_lists
     * @param  const            $priceListsKey
     * @param  String/Integer   $newPrice      
     * @param  boolean          $basePrice     
     * @param  boolean/Member   $member        
     * @return Object                 
     */
    public function updatePrice($priceListsKey, $newPrice, $basePrice, $member = false, $allUpdate = false)
    {
    	$priceList = PriceList::where('name', '=', $priceListsKey)->first();
    	if($priceList){
            $query = $this->prices()->whereHas('priceList', function($q) use($priceListsKey){
                return $q->where('name', '=', $priceListsKey);
            });

    		if($basePrice){
                if($allUpdate == false){
                    $existingPrice = $query->whereNull('user_id')->first();
                    $price = $this->updateNowProduct($existingPrice, $newPrice, $priceList, $member, $allUpdate);
                } else {
                    $existingPrices = $query->get();
                    foreach($existingPrices as $item){
                        $price = $this->updateNowProduct($item, $newPrice, $priceList, $member, $allUpdate);
                    }
                }

    			return $price;
    		} else {
    			$existingPrice = $query->where('user_id', '=', $member->id)->first();
    			$price = $this->updateNowProduct($existingPrice, $newPrice, $priceList, $member);

    			return $price;
    		}
    	}

    	return false;
    }

    public function updateNowProduct($existingPrice, $newPrice, $priceList, $member = false, $allUpdate = false)
    {
    	if($existingPrice != null){
    		$price = $existingPrice;
    	} else {
    		$price = new ProductPrice;
    	}
        if($member != false){
            $price->user_id = $member->id;
            if(is_numeric($newPrice)){
                $price->member_price = $newPrice;
                $price->is_tba = false;
            } else {
                $price->is_tba = true;
            }
        } else {
            if(is_numeric($newPrice)){
                $price->base_price = $newPrice;
                $price->is_tba = false;
                if($allUpdate){
                    $price->member_price = $newPrice;
                }
            } else {
                $price->is_tba = true;
            }
        }

    	if($this instanceof Product){
    		$price->product_id = $this->id;
    	} else if($this instanceof Combination){
    		$price->combination_id = $this->id;
    	}
    	$price->price_list_id = $priceList->id;
    	$price->save();

    	return $price;
    }

    /**
     * Get this product price
     */
    public function getPriceList($priceList, $member = false)
    {
        $query = $this->prices()->whereHas('priceList', function($q) use($priceList){
            return $q->where('name', '=', $priceList);
        });

        if($member){
            $query = $query->where('user_id', '=', $member->id);  
        } else {
            $query = $query->whereNull('user_id');
        }

        $price = $query->first();
        return $price != null ? $price : false;
    }
}
