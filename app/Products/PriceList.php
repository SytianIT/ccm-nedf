<?php

namespace App\Products;

use App\Products\ProductPrice;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    protected $table = 'price_lists';

    const WHOLESALE = 'wholesale';
    const TRADE_DISCOUNT = 'trade_discount';
    const TRADE = 'trade';
    const RETAIL = 'retail';

    public function products()
    {
    	return $this->hasMany(ProductPrice::class, 'price_list_id');
    }

    public function members()
    {
    	return $this->hasMany(User::class, 'price_list_id');
    }
}
