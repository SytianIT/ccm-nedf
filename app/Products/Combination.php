<?php

namespace App\Products;

use App\Products\BaseModel;
use App\Products\ProductPrice;

class Combination extends BaseModel
{
    protected $table = 'combination_pricing';

    public function prices()
    {
    	return $this->hasMany(ProductPrice::class, 'combination_id');
    }
}
