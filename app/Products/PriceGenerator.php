<?php

namespace App\Products;

use App\Products\BaseModel;

class PriceGenerator extends BaseModel
{
    public function generatePriceFromThisRequest($request, $counter, $member)
    {
    	// Check first selection in column 1
    	$price = 0;
    	$col1 = $request->doors_frames_selection[$counter];
    	$priceList = $member->priceList;
    	$isTBA = false;

    	// Price generation for selection of col 1 equals = FRAME | FRAME & DOOR
    	if($col1 == Product::FRAME || $col1 == Product::FRAME_DOOR){
    		$colFiveTen = $request->door_thick_or_rebate_size[$counter];
    		if($colFiveTen == Product::DOOR_RADIO){
    			$col5 = $this->getSelectFieldPrice($request->door_height_first[$counter], $member, $priceList, 'price');
    			$col6 = $this->getSelectFieldPrice($request->door_width_first[$counter], $member, $priceList, 'price');
    			$col7 = $this->getSelectFieldPrice($request->door_thick_first[$counter], $member, $priceList, 'price');
    			if($col5 == 'TBA' || $col6 == 'TBA' || $col6 == 'TBA'){
    				$isTBA = true;
    			} else {
    				$price = $price + $col5 + $col6 + $col7;
    			}
    		} else if($colFiveTen == Product::REVEAL_RADIO){
    			$col8 = $this->getSelectFieldPrice($request->reveal_height[$counter], $member, $priceList, 'price');
    			$col9 = $this->getSelectFieldPrice($request->reveal_width[$counter], $member, $priceList, 'price');
    			$col10 = $request->reveal_radio_option[$counter];
    			if($col10 == Product::DOOR_THICK){
    				$col10Sub = $this->getSelectFieldPrice($request->reveal_door_thick_drop[$counter], $member, $priceList, 'price');
    			} else if($col10 == Product::REBATE_SIZE){
    				$col10Sub = $this->getSelectFieldPrice($request->reveal_rebate_size_drop[$counter], $member, $priceList, 'price');
    			}
    			if($col8 == 'TBA' || $col9 == 'TBA' || $col10Sub == 'TBA'){
    				$isTBA = true;
    			} else {
    				$price = $price + $col5 + $col6 + $col7;
    			}
    		}
    		$col16 = $this->getSelectFieldPrice($request->storm_mould[$counter], $member, $priceList, 'price');
    		$col18 = $this->getSelectFieldPrice($request->frame_fixing[$counter], $member, $priceList, 'price');
    		$col19 = $this->getSelectFieldPrice($request->wall_finishes[$counter], $member, $priceList, 'price');
    		$col20 = $this->getSelectFieldPrice($request->hinge_type[$counter], $member, $priceList, 'price');
    		$col21 = $this->getSelectFieldPrice($request->hinge_size[$counter], $member, $priceList, 'price');
    		$col22 = ($request->hinge_qty[$counter] / Product::HINGE_QTY_DIVISION) * Product::HINGE_QTY;
    		$col24 = $this->getSelectFieldPrice($request->hinge_fixing[$counter], $member, $priceList, 'price');
    		if($request->lock_type[$counter] != Product::LOCK_TYPE_ELECTRIC){
    			$col25 = $this->getSelectFieldPrice($request->lock_type[$counter], $member, $priceList, 'price');
    		} else {
    			$col25 = 'TBA';
    		}
    		$col26 = $this->getSelectFieldPrice($request->lock_height_striker[$counter], $member, $priceList, 'price');
    		$col27 = $this->getSelectFieldPrice($request->closer[$counter], $member, $priceList, 'price');
    		$col28 = $this->getSelectFieldPrice($request->kd_welded[$counter], $member, $priceList, 'price');
    		$col29 = $this->getSelectFieldPrice($request->priming_powder_coating[$counter], $member, $priceList, 'price');
    		if($col16 == 'TBA' || $col18 == 'TBA' || $col19 == 'TBA' || $col20 == 'TBA' || $col21 == 'TBA' || $col22 == 'TBA' || $col24 == 'TBA' || $col25 == 'TBA' || $col26 == 'TBA' || $col27 == 'TBA' || $col28 == 'TBA' || $col29 == 'TBA'){
				$isTBA = true;
			} else {
				$price = $price + $col16 + $col18 + $col19 + $col20 + $col21 + $col22 + $col24 + $col25 + $col26 + $col27 + $col28 + $col29;
			}
    	}

    	// Price generation for selection of col 1 equals = DOOR | FRAME & DOOR
    	if($col1 == Product::DOOR || $col1 == Product::FRAME_DOOR){
    		$col30 = $this->getSelectFieldPrice($request->door_type_second[$counter], $member, $priceList, 'price');
			$col31 = $this->getSelectFieldPrice($request->door_height_second[$counter], $member, $priceList, 'price');
			$col32 = $this->getSelectFieldPrice($request->door_width_second[$counter], $member, $priceList, 'price');
			$col33 = $this->getSelectFieldPrice($request->door_thick_second[$counter], $member, $priceList, 'price');
			if($col30 == 'TBA' || $col31 == 'TBA' || $col32 == 'TBA' || $col33 == 'TBA'){
				$isTBA = true;
			} else {
				$price = $price + $col30 + $col31 + $col32 + $col33;
			}
    	}

    	return $isTBA ? 'TBA' : $price;
    }

    public function getSelectFieldPrice($value, $member, $priceList, $returnType)
    {
    	if($value != 'custom'){
	    	$isBase = false;
	    	$product = Product::where('name', '=', $value)->first();
	    	$price = $product->getPriceList($priceList->name, $member);
	    	if($price == false){
	    		$isBase = true;
	    		$price = $product->getPriceList($priceList->name);
	    	}

	    	if($price != false){
		    	if($returnType == 'price'){
		    		if($isBase){
		    			return $price->is_tba ? 'TBA' : $price->base_price;
		    		} else {
		    			return $price->is_tba ? 'TBA' : $price->member_price;
		    		}
		    	} else {
		    		return $price;
		    	}
		    }

		    return 0;
	    }

	    return 'TBA';
    }

    public function getPriceFromThisColumnCombination($request)
    {
        
    }
}
