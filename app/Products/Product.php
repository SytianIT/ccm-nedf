<?php

namespace App\Products;

use App\Products\BaseModel;
use App\Products\ProductPrice;

class Product extends BaseModel
{
    protected $table = 'products';

    const FRAME = 'f';
    const DOOR = 'd';
    const FRAME_DOOR = 'f-d';

    const DOOR_RADIO = 'door';
    const REVEAL_RADIO = 'reveal';

    const DOOR_THICK = 'door_thick';
    const REBATE_SIZE = 'rebate_size';

    const HINGE_QTY = 8;
    const HINGE_QTY_DIVISION = 3;

    const LOCK_TYPE_ELECTRIC = 'electric';

    public static $mainProducts = [
        self::FRAME => 'F',
        self::DOOR => 'D',
        self::FRAME_DOOR => 'F & D'
    ];

    public static $colFiveTen = [
        self::DOOR_RADIO => 'Door',
        self::REVEAL_RADIO => 'Reveal'
    ];

    public static $colTen = [
        self::DOOR_THICK => 'Door Thick',
        self::REBATE_SIZE => 'Rebate Size'
    ];

    public function prices()
    {
    	return $this->hasMany(ProductPrice::class, 'product_id');
    }

    // Scope Query
    public function scopeDisplay($query)
    {
    	return $query->where('display', '=', true);
    }

    public function scopeParent($query)
    {
    	return $query->whereNull('parent_id');
    }

    // Getters and Setters
    public function getChildsAttribute()
    {
    	$childs = Product::where('parent_id', '=', $this->id)->get();

    	return $childs;
    }

    /**
     * Other Methods
     */
    public function getQuoteDisplayProducts()
    {
        $products = [];
        $queryProds = $this->display()->parent()->get();
        foreach($queryProds as $prod){
            $data = [];
            foreach($prod->childs as $child){
                $data = array_add($data, $child->name, $child->title);
            }
            $products[$prod->name] = $data;
        }

        $products = (object) $products;
        return $products;
    }
}
