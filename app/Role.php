<?php

namespace App;

use App\BaseModel;
use App\Permission;
use App\User;

class Role extends BaseModel
{
    /**
     * Role Eloquent Relations
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles', 'role_id', 'user_id');
    }

    public function permissions()
    {
    	return $this->belongsToMany(Permission::class, 'role_permissions', 'role_id', 'permission_id');
    }
}
