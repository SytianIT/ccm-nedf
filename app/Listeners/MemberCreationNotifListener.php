<?php

namespace App\Listeners;

use App\Events\MemberCreationNotif;
use App\SiteContent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;

class MemberCreationNotifListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  MemberCreationNotif  $event
     * @return void
     */
    public function handle(MemberCreationNotif $event)
    {
        $contents = SiteContent::findOrFail(1);
        $member = $event->member;
        $password = $event->password;

        $data = [
            'member' => $member,
            'password' => $password,
            'site_email' => $contents->site_email
        ];

        $this->mailer->send('emails.member-creation-notification', $data, function ($m) use ($member, $contents) {
            $m->from($contents['email'], $contents['title']);
            $m->to($member->email, $member->firstname)->subject('North Eastern Door Frames - Member Account Created.');
        });
    }
}
