<?php

namespace App\Listeners;

use App\Events\MemberUpdatedNotif;
use App\SiteContent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;

class MemberUpdatedNotifListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  MemberUpdatedNotif  $event
     * @return void
     */
    public function handle(MemberUpdatedNotif $event)
    {
        $contents = SiteContent::findOrFail(1);
        $member = $event->member;

        $data = [
            'member' => $member,
            'site_email' => $contents->site_email
        ];

        $this->mailer->send('emails.member-update-notification', $data, function ($m) use ($member, $contents) {
            $m->from($contents['email'], $contents['title']);
            $m->to($member->email, $member->firstname)->subject('North Eastern Door Frames - Account Changes.');
        });
    }
}
