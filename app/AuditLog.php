<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    public function auditable()
    {
    	return $this->morphTo();
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
