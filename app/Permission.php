<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * Permission Eloquent Relations
     */
    public function roles()
    {
    	return $this->belongsToMany(Role::class, 'role_permissions', 'permission_id', 'role_id');
    }

    /**
     * Scope Queries
     */
    public function scopeAssignable($query)
    {
    	return $query->whereNotIn('id', [1,2]);
    }
}
