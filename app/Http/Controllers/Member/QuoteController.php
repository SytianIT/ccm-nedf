<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Products\PriceGenerator;
use App\Products\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuoteController extends Controller
{
    public function index()
    {
    	return view('account.member.quote.index');
    }

    public function create()
    {
    	$product = new Product;

    	$quoteNum = intval(microtime(true));
    	$col1 = Product::$mainProducts;
    	$products = $product->getQuoteDisplayProducts();
    	$colFiveTen = Product::$colFiveTen;
    	$colTen = Product::$colTen;

    	return view('account.member.quote.create', compact('col1','quoteNum','products','colFiveTen','colTen'));
    }

    public function getPrice(Request $request)
    {
    	$member = Auth::user();
    	$generator = new PriceGenerator;
    	$price = $generator->generatePriceFromThisRequest($request, $request->counter, $member);

    	if($price != null){
    		$response['success'] = true;
    		$response['price'] = $price;

    		return response()->json($response);
    	}

    	return response()->json(['success' => false, 'message' => 'There was an error in processing the request, please try again later.']);
    }
}
