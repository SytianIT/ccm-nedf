<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function index()
    {
    	return view('account.member.index');
    }

    public function profile(User $user)
    {
        return view('account.member.profile');
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $data = [
            'firstname' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'username' => 'required|max:15|unique:users,username,'.$user->id,
        ];

        if($request->has('password')){
            $data = array_merge($data, [
                        'password' => 'required|max:15|min:5|confirmed',
                        'password_confirmation' => 'required'
                    ]);
        }

        $this->validate($request, $data);

        if($request->hasFile('image')){
            $user->profile_img = $this->baseUpload($request->file('image'), 'uploads/members', 150, 150);
        }
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->contact_number = $request->contact_number;
        $user->email = $request->email;
        
        if($request->has('password')){
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect()->back()
                         ->withSuccess('Profile updated successfully');
    }
}
