<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Products\PriceList;
use App\Products\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{
    public function index()
    {
    	$members = User::members()->orderBy('created_at', 'DESC')->get();

    	return view('account.admin.member.index', compact('members'));
    }

    public function create()
    {
        $priceLists = PriceList::orderBy('created_at', 'DESC')->get();

    	return view('account.admin.member.create', compact('priceLists'));
    }

    public function store(Request $request)
    {
    	$this->validateMemberFields($request);

    	$member = new User;
    	$member = $this->fillBasicMemberFields($request, $member);
    	$member->roles()->sync([2]);
    	if($member){
    		event(new \App\Events\MemberCreationNotif($member, $request->password));
    	}
    	$member->insertAuditLog("New member with username '{$member->username}' has been created");

    	return redirect()->route('members')
    					 ->withSuccess('New member successfully created.');
    }

    public function edit(User $member)
    {
        $priceLists = PriceList::orderBy('created_at', 'DESC')->get();

    	return view('account.admin.member.edit', compact('member','priceLists'));
    }

    public function update(Request $request, User $member)
    {
    	$this->validateMemberFields($request, $member);

    	$member = $this->fillBasicMemberFields($request, $member);

    	if($request->has('notify_merchant')){
    		event(new \App\Events\MemberUpdatedNotif($member));
    	}
    	$member->insertAuditLog("A member details with username '{$member->username}' has been updated");

    	return redirect()->route('members')
    					 ->withSuccess("Member '{$member->username}' updated successfully.");
    }

    public function remove(User $member)
    {
    	$username = $member->username;
    	$member->delete();
    	$member->insertAuditLog("A member with username '{$member->username}' has been deleted");

    	return redirect()->route('members')
    					 ->withWarning("Member '{$username}' deleted successfully.");
    }

    private function validateMemberFields($request, $member = null)
    {
    	$passwordFields = [
            'password' => 'required|max:15|min:5|confirmed',
            'password_confirmation' => 'required'
        ];

        $id = ($member != null) ? ','.$member->id : '';
        $basicFields = [
            'firstname' => 'required|max:255',
            'email' => 'required|email|unique:users,email'.$id,
            'username' => 'required|max:15|unique:users,username'.$id,
            'price_list_id' => 'required',
        ];

        if($member != null){
            if($request->has('password')){
                $basicFields = array_merge($basicFields, $passwordFields);
            }
        }

        if($member == null){
            $basicFields = array_merge($basicFields, $passwordFields);
        }

        $this->validate($request, $basicFields, [
            'firstname.required' => 'Your first name is required',
            'lastname.required' => 'Your last name is required',
        ]);
        return;
    }

    public function fillBasicMemberFields($request, $member)
    {
    	$member->firstname = $request->firstname;
    	$member->lastname = $request->lastname;
    	$member->address = $request->address;
    	$member->contact_number = $request->contact_number;
    	$member->email = $request->email;
    	$member->username = $request->username;
        if($request->has('price_list_id')){
            $member->price_list_id = $request->price_list_id;
        }

    	if($request->has('password')){
    		$member->password = Hash::make($request->password);
    	}	

    	$member->save();
    	return $member;
    }

    public function editPriceLists(User $member)
    {
        $products = Product::display()->parent()->get();
        $priceLists = PriceList::orderBy('created_at', 'DESC')->get();

        return view('account.admin.member.edit-price', compact('products','priceLists','member'));
    }

    public function getPriceLists(Request $request, User $member)
    {
        $products = Product::display()->parent()->get();
        $priceLists = PriceList::orderBy('created_at', 'DESC')->get();
        $selectedPriceList = PriceList::find($request->price_list_key);

        return view('account.admin.member.inc.ajax-price-lists-table', compact('products','priceLists','member','selectedPriceList'));
    }

    public function updatePriceLists(Request $request, User $member)
    {
        $this->validate($request, [
            'price_list_id' => 'required'
        ]);

        $member->price_list_id = $request->price_list_id;
        $member->save();

        if($member->priceList != null){
            $products = Product::display()->parent()->get();
            foreach($products as $product){
                $childs = Product::where('parent_id', '=', $product->id)->get();
                $name = $product->name;
                foreach($request->$name as $key => $item){
                    $firstKey = array_keys($item);
                    $firstValue = reset($item);
                    if($firstKey[0] != ''){
                        $subProduct = Product::where('name', '=', $firstKey[0])->first();
                        $existingPriceList = $member->prices()->where('product_id', '=', $subProduct->id)->first();
                        $price = $subProduct->updateNowProduct($existingPriceList, $item[$member->priceList->name], $member->priceList, $member);
                    }
                }
            }
        }

        return redirect()->route('member.edit.price-lists', $member->id)
                         ->withSuccess('Member price list updated successfully');
    }   
}
