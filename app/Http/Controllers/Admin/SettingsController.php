<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Permission;
use App\Role;
use App\SiteContent;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function indexRoles()
    {
    	$roles = Role::orderBy('created_at', 'DESC')->whereNotIn('id', [1,2])->get();

    	return view('account.admin.role.index', compact('roles'));
    }

    public function createRole()
    {
    	$permissions = Permission::orderBy('created_at', 'ASC')->assignable()->get();

    	return view('account.admin.role.create', compact('permissions'));
    }

    public function storeRole(Request $request)
    {
    	$this->validateRoleFields($request);

    	$role = new Role;
    	$role = $this->fillBasicRoleFields($request, $role);
        $role->insertAuditLog("New role entitled '{$role->title}' has been created");

    	return redirect()->route('roles')
    					 ->withSuccess('New role successfully created.');
    }

    public function editRole(Role $role)
    {
    	$permissions = Permission::orderBy('created_at', 'ASC')->assignable()->get();

    	return view('account.admin.role.edit', compact('permissions','role'));
    }

    public function updateRole(Request $request, Role $role)
    {
    	$this->validateRoleFields($request);

    	$role = $this->fillBasicRoleFields($request, $role);
        $role->insertAuditLog("Role details entitled '{$role->title}' has been updated");

    	return redirect()->route('roles')
    					 ->withSuccess("Role '{$role->title}' updated successfully.");
    }

    public function removeRole(Role $role)
    {
    	$title = $role->title;
        $role->users()->detach();
        $role->permissions()->detach();
    	$role->delete();
        $role->insertAuditLog("Role entitled '{$title}' has been deleted");

    	return redirect()->route('roles')
    					 ->withWarning("Role '{$role->title}' deleted successfully.");
    }

    private function validateRoleFields($request)
    {
    	$this->validate($request, [
    		'title' => 'required|max:255'
    	]);

    	return;
    }

    private function fillBasicRoleFields($request, $role)
    {
        $permissions = array_merge($request->permissions, [2]);
    	$role->name = str_slug($request->title, $separator = "-");
    	$role->title = $request->title;
    	$role->description = $request->description;
    	$role->save();
    	$role->permissions()->sync($permissions);

    	return $role;
    }

    /**
     * START FUNCTIONS FOR ADMINS
     */
    public function indexUsers()
    {
        $users = User::admins()->whereNotIn('id', [1])->orderBy('created_at', 'DESC')->paginate(25);

        return view('account.admin.admins.index', compact('users'));
    }

    public function createUser()
    {
        $roles = Role::whereNotIn('id', [1,2])->get();

        return view('account.admin.admins.create', compact('roles'));
    }

    public function storeUser(Request $request)
    {
        $this->validateUserFields($request);

        $user = new User;
        $user = $this->fillBasicUserFields($request, $user);
        $user->insertAuditLog("New admin with username '{$user->username}' has been created");

        return redirect()->route('users')
                         ->withSuccess("New admin user successfully created.");
    }

    public function editUser(User $user)
    {
        $roles = Role::whereNotIn('id', [1,2])->get();

        return view('account.admin.admins.edit', compact('roles','user'));
    }

    public function updateUser(Request $request, User $user)
    {
        $this->validateUserFields($request, $user);

        $user = $this->fillBasicUserFields($request, $user);
        $user->insertAuditLog("Admin user details with username '{$user->username}' has been updated");

        return redirect()->route('users')
                         ->withSuccess("Admin user '{$user->username}' updated successfully.");
    }

    public function removeUser(User $user)
    {
        $username = $user->username;
        $user->delete();
        $user->insertAuditLog("Admin user with username '{$username}' has been deleted");

        return redirect()->route('users')
                         ->withWarning("Admin user '{$username}' deleted successfully.");
    }

    private function validateUserFields($request, $user = null)
    {
        $passwordFields = [
            'password' => 'required|max:15|min:5|confirmed',
            'password_confirmation' => 'required'
        ];

        $id = ($user != null) ? ','.$user->id : '';
        $basicFields = [
            'firstname' => 'required|max:255',
            'email' => 'required|email|unique:users,email'.$id,
            'username' => 'required|max:15|unique:users,username'.$id,
            'role_id' => 'required'
        ];

        if($user != null){
            if($request->has('password')){
                $basicFields = array_merge($basicFields, $passwordFields);
            }
        }

        if($user == null){
            $basicFields = array_merge($basicFields, $passwordFields);
        }

        $this->validate($request, $basicFields, [
            'role_id.required' => 'Please assign roles for this user.',
            'firstname.required' => 'Your first name is required',
            'lastname.required' => 'Your last name is required',
        ]);
        return;
    }

    private function fillBasicUserFields($request, $user)
    {
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->contact_number = $request->contact_number;
        $user->email = $request->email;
        
        if($request->has('password')){
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
        }
        
        $user->save();
        $user->roles()->sync($request->role_id);

        return $user;
    }

    /**
     * START FUNCTIONS FOR PROFILE
     */
    public function profile(User $user)
    {
        return view('account.admin.profile');
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $data = [
            'firstname' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'username' => 'required|max:15|unique:users,username,'.$user->id,
        ];

        if($request->has('password')){
            $data = array_merge($data, [
                        'password' => 'required|max:15|min:5|confirmed',
                        'password_confirmation' => 'required'
                    ]);
        }

        $this->validate($request, $data);

        if($request->hasFile('image')){
            $user->profile_img = $this->baseUpload($request->file('image'), 'uploads/admins', 150, 150);
        }
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->contact_number = $request->contact_number;
        $user->email = $request->email;
        
        if($request->has('password')){
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect()->back()
                         ->withSuccess('Profile updated successfully');
    }

    /**
     * START FUNCTIONS FOR SITE SETTINGS
     */
    public function settings()
    {
        $contents = SiteContent::findOrFail(1);

        return view('account.admin.settings.index', compact('contents'));
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'site_title' => 'required|max:255',
        ]);

        $contents = SiteContent::findOrFail(1);
        $contents->email = $request->email;
        $contents->site_title = $request->site_title;
        $contents->is_active = ($request->enabled == true) ? true : false;
        $contents->save();
        $contents->insertAuditLog("Site details has been updated");
        
        return redirect()->back()
                         ->withSuccess('Site settings updated successfully.');
    }

    public function auditLogs()
    {
        $logs = AuditLog::orderBy('created_at', 'DESC')->paginate(50);

        return view('account.admin.settings.audit-logs', compact('logs'));
    }
}
