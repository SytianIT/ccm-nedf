<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Products\Combination;
use App\Products\FrameProfile;
use App\Products\PriceList;
use App\Products\Product;
use Illuminate\Http\Request;

class PriceListController extends Controller
{
    /**
     * Methods for product price lists
     */
    public function index()
    {
    	$products = Product::display()->parent()->get();
    	$priceLists = PriceList::orderBy('created_at', 'DESC')->get();

    	return view('account.admin.price.index', compact('priceLists','products'));
    }

    public function updatePriceLists(Request $request)
    {
        $safeSubProd = [];
    	$products = Product::display()->parent()->get();
    	foreach($products as $product){
            $childs = Product::where('parent_id', '=', $product->id)->get();
    		$name = $product->name;
    		foreach($request->$name as $key => $item){
    			$firstKey = array_keys($item);
                $firstValue = reset($item);
    			if($firstKey[0] != ''){
    				$subProduct = Product::where('name', '=', $firstKey[0])->first();
    				if($subProduct != null){
                        $allUpdate = false;
                        if($request->has('update_members_price')){
                            $allUpdate = true;
                        }
                        $safeSubProd[] = $subProduct->id;
                        $this->updateAllPrice($subProduct, $item, true, false, $allUpdate);
                        $subProduct->title = $firstValue;
                        $subProduct->save();
    				} else {
                        $subProduct = new Product;
                        $subProduct->name = $this->makeSlugProductName($firstValue, 1);
                        $subProduct->title = $firstValue;
                        $subProduct->parent_id = $product->id;
                        $subProduct->default = false;
                        $subProduct->save();
                        $this->updateAllPrice($subProduct, $item, true);
                    }
    			}
    		}
            $childs->each(function($q) use($safeSubProd){
                if(!in_array($q->id, $safeSubProd)){
                    Product::find($q->id)->delete();
                }
            });
    	}

        return redirect()->back()
                         ->withSuccess('Price lists updated successfully.');
    }

    private function makeSlugProductName($name, $ctr)
    {
        $oldName = $name;
        $name = $name.'-'.$ctr;
        $product = Product::where('name', '=', $name)->get();
        if($product->count() > 0){
            $ctr++;
            return $this->makeSlugProductName($oldName, $ctr);
        }

        return $name;
    }

    /**
     * Methods for combination price lists
     */

    public function combinationsPriceLists()
    {
        $hands = Product::where('name', '=', 'hand_form')->first();
        $handChilds = $hands->childs;
        $gauge = Product::where('name', '=', 'gauge_material')->first();
        $gaugeChilds = $gauge->childs;
        $priceLists = PriceList::orderBy('created_at', 'DESC')->get();
        $combinations = Combination::orderBy('created_at', 'ASC')->get();

        return view('account.admin.price.combinations', compact('handChilds','gaugeChilds','priceLists','combinations'));
    }

    public function combinationsPriceListsUpdate(Request $request)
    {
        if($request->combination != null){
            foreach($request->combination as $combination){
                if($combination['id'] != ''){
                    $combinationItem = Combination::find($combination['id']);
                } else {
                    $combinationItem = new Combination;
                }

                $combinationItem->col_11 = intval($combination['col_11']);
                $combinationItem->col_12 = intval($combination['col_12']);
                $combinationItem->col_13 = $combination['col_13'];
                $combinationItem->col_14 = $combination['col_14'];
                $combinationItem->col_23 = intval($combination['col_23']);
                $combinationItem->save();
                $this->updateAllPrice($combinationItem, $combination, true);
            }

            return redirect()->back()
                             ->withSuccess('Combination price lists updated successfully.');
        }

        return redirect()->back()
                         ->withSuccess('There was an error in processing the request, please try again later.');
    }

    private function updateAllPrice($model, $item, $base, $member = false, $allUpdate = false)
    {
        $model->updatePrice(PriceList::WHOLESALE, $item[PriceList::WHOLESALE], true, $member, $allUpdate);
        $model->updatePrice(PriceList::TRADE_DISCOUNT, $item[PriceList::TRADE_DISCOUNT], true, $member, $allUpdate);
        $model->updatePrice(PriceList::TRADE, $item[PriceList::TRADE], true, $member, $allUpdate);
        $model->updatePrice(PriceList::RETAIL, $item[PriceList::RETAIL], true, $member, $allUpdate);

        return true;
    }

    /**
     * Frame Profiles Methods
     */
    public function frameProfiles()
    {
        $frames = FrameProfile::orderBy('created_at', 'DESC')->get(); 

        return view('account.admin.price.frame-profiles.index', compact('frames'));
    }

    public function createFrameProfile()
    {
        return view('account.admin.price.frame-profiles.create');
    }

    public function storeFrameProfile(Request $request)
    {
        $this->validateFrameProfileBasicField($request);

        $frame = new FrameProfile;
        if($request->hasFile('image')){
            $frame->path = $this->baseUpload($request->file('image'), 'uploads/frames', 1200, 760);
        }
        $frame->title = $request->title;
        $frame->description = $request->description;
        $frame->save();

        return redirect()->route('admin.frame-profiles')
                         ->withSuccess('Frame profile created successfully.');
    }  

    public function editFrameProfile(FrameProfile $frame)
    {
        return view('account.admin.price.frame-profiles.edit', compact('frame'));
    }   

    public function updateFrameProfile(Request $request, FrameProfile $frame)
    {
        $this->validateFrameProfileBasicField($request, $frame);

        if($request->hasFile('image')){
            $frame->path = $this->baseUpload($request->file('image'), 'uploads/frames', 1200, 760);
        }
        $frame->title = $request->title;
        $frame->description = $request->description;
        $frame->save();

        return redirect()->route('admin.frame-profiles')
                         ->withSuccess('Frame profile updated successfully.');
    } 

    public function deleteFrameProfile(FrameProfile $frame)
    {
        $name = $frame->title;
        $frame->delete();

        return redirect()->route('admin.frame-profiles')
                         ->withSuccess("Frame profile '{$name}' deleted successfully.");
    }

    private function validateFrameProfileBasicField($request, $frame = null)
    {
        $data = [
            'title' => 'required|max:255'
        ];

        if($frame == null){
            $data = array_add($data, 'image', 'required|image|max:5000');
        }
        $this->validate($request, $data);

        return;
    }
}
