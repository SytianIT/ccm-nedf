<?php

namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	protected function baseUpload(UploadedFile $image = null, $path = null, $width = null, $height = null)
	{
		if ($image && $image->isValid()) {
			$validator = Validator::make(
				['image' => $image],
				['image' => 'required|image']
			);

			if ($validator->fails()) {
				return null;
			}

			$fileName = md5(microtime()).'.'.$image->getClientOriginalExtension();
			$uploadPath = public_path($path.'/'.$fileName);
			$img = Image::make($image->getRealPath());

			if($width && $height) {
				$img->fit($width, $height)->save($uploadPath);
			} else if ($width) {
				$img->resize($width, null, function ($constraint) {
					$constraint->aspectRatio();
				})->save($uploadPath);
			} else if ($height) {
				$img->resize(null, $height, function ($constraint) {
					$constraint->aspectRatio();
				})->save($uploadPath); 
			} else {
				$img->save(public_path($path), $fileName);
			}

			return $path.'/'.$fileName;
		}

		return null;
	}
}
