<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('login', ['uses' => 'Auth\AuthController@showLoginForm', 'as' => 'show.login']);
Route::post('login', ['uses' => 'Auth\AuthController@login', 'as' => 'auth.login']);

Route::get('register', ['uses' => 'Auth\AuthController@showRegistrationForm', 'as' => 'show.registration']);
Route::post('register', ['uses' => 'Auth\AuthController@register', 'as' => 'auth.register']);

Route::post('password/email', ['uses' => 'Auth\PasswordController@sendResetLinkEmail', 'as' => 'send.reset.link']);
Route::post('password/email/validate', ['uses' => 'Auth\PasswordController@validateEmail', 'as' => 'password.reset.validate.email']);
Route::get('password/reset/{token?}', ['uses' => 'Auth\PasswordController@showResetForm', 'as' => 'show.password.reset']);
Route::post('password/reset', ['uses' => 'Auth\PasswordController@reset', 'as' => 'password.reset']);

Route::get('under-maintenance', ['uses' => 'HomeController@underMaintenance', 'as' => 'under.maintenance']);
Route::get('logout', ['uses' => 'Auth\AuthController@logout', 'as' => 'auth.logout', 'middleware' => 'auth']);

Route::group(['middleware' => ['auth','system']], function(){

	Route::get('/', function(){
		return redirect()->route('admin');
	});

	/**
	 * Admin Routes
	 */
	Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function(){
		Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'admin']);

		// Admin Profile
		Route::get('profile', ['uses' => 'SettingsController@profile', 'as' => 'admin.profile']);
		Route::post('profile/update', ['uses' => 'SettingsController@updateProfile', 'as' => 'admin.profile.update']);

		// Site Settings
		Route::get('settings', ['uses' => 'SettingsController@settings', 'as' => 'admin.settings']);
		Route::post('settings/update', ['uses' => 'SettingsController@updateSettings', 'as' => 'admin.settings.update']);

		// Audit Logs
		Route::get('audit-logs', ['uses' => 'SettingsController@auditLogs', 'as' => 'admin.audit-logs']);

		// Price Lists
		Route::get('price-lists', ['uses' => 'PriceListController@index', 'as' => 'admin.price-lists']);
		Route::post('price-lists/update', ['uses' => 'PriceListController@updatePriceLists', 'as' => 'admin.price-lists.update']);
		Route::get('price-combinations', ['uses' => 'PriceListController@combinationsPriceLists', 'as' => 'admin.price-lists.combinations']);
		Route::post('price-combinations/store', ['uses' => 'PriceListController@combinationsPriceListsUpdate', 'as' => 'admin.price-lists.combinations.update']);
		Route::get('frame-profiles', ['uses' => 'PriceListController@frameProfiles', 'as' => 'admin.frame-profiles']);
		Route::get('frame-profile/create', ['uses' => 'PriceListController@createFrameProfile', 'as' => 'admin.frame-profile.create']);
		Route::post('frame-profile/store', ['uses' => 'PriceListController@storeFrameProfile', 'as' => 'admin.frame-profile.store']);
		Route::get('frame-profile/{frame}/edit', ['uses' => 'PriceListController@editFrameProfile', 'as' => 'admin.frame-profile.edit']);
		Route::post('frame-profile/{frame}/update', ['uses' => 'PriceListController@updateFrameProfile', 'as' => 'admin.frame-profile.update']);
		Route::get('frame-profile/{frame}/delete', ['uses' => 'PriceListController@deleteFrameProfile', 'as' => 'admin.frame-profile.delete']);

		// Roles Routes
		Route::get('roles', ['uses' => 'SettingsController@indexRoles', 'as' => 'roles']);
		Route::get('role/create', ['uses' => 'SettingsController@createRole', 'as' => 'role.create']);
		Route::post('role/store', ['uses' => 'SettingsController@storeRole', 'as' => 'role.store']);
		Route::get('role/{role}/edit', ['uses' => 'SettingsController@editRole', 'as' => 'role.edit']);
		Route::post('role/{role}/update', ['uses' => 'SettingsController@updateRole', 'as' => 'role.update']);
		Route::get('role/{role}/remove', ['uses' => 'SettingsController@removeRole', 'as' => 'role.delete']);

		// Admins Roles
		Route::get('users', ['uses' => 'SettingsController@indexUsers', 'as' => 'users']);
		Route::get('user/create', ['uses' => 'SettingsController@createUser', 'as' => 'user.create']);
		Route::post('user/store', ['uses' => 'SettingsController@storeUser', 'as' => 'user.store']);
		Route::get('user/{user}/edit', ['uses' => 'SettingsController@editUser', 'as' => 'user.edit']);
		Route::post('user/{user}/update', ['uses' => 'SettingsController@updateUser', 'as' => 'user.update']);
		Route::get('user/{user}/remove', ['uses' => 'SettingsController@removeUser', 'as' => 'user.delete']);		

		// Members Roles
		Route::get('members', ['uses' => 'MemberController@index', 'as' => 'members']);
		Route::get('member/create', ['uses' => 'MemberController@create', 'as' => 'member.create']);
		Route::post('member/store', ['uses' => 'MemberController@store', 'as' => 'member.store']);
		Route::get('member/{member}/edit', ['uses' => 'MemberController@edit', 'as' => 'member.edit']);
		Route::post('member/{member}/update', ['uses' => 'MemberController@update', 'as' => 'member.update']);
		Route::get('member/{member}/price-list', ['uses' => 'MemberController@editPriceLists', 'as' => 'member.edit.price-lists']);
		Route::post('member/{member}/price-list/get', ['uses' => 'MemberController@getPriceLists', 'as' => 'member.get.price-lists']);
		Route::post('member/{member}/price-list/update', ['uses' => 'MemberController@updatePriceLists', 'as' => 'member.update.price-lists']);
		Route::get('member/{member}/remove', ['uses' => 'MemberController@remove', 'as' => 'member.delete']);	
	});

	/**
	 * Member Routes
	 */
	Route::group(['middleware' => 'member', 'prefix' => 'member', 'namespace' => 'Member'], function(){
		Route::get('dashboard', ['uses' => 'DashboardController@index', 'as' => 'member']);

		// Member Profile
		Route::get('profile', ['uses' => 'DashboardController@profile', 'as' => 'member.profile']);
		Route::post('profile', ['uses' => 'DashboardController@updateProfile', 'as' => 'member.profile.update']);

		// Quotation Controller
		Route::get('quotes', ['uses' => 'QuoteController@index', 'as' => 'member.quotes']);
		Route::get('quote/create', ['uses' => 'QuoteController@create', 'as' => 'member.quote.create']);
		Route::post('quote/price/get', ['uses' => 'QuoteController@getPrice', 'as' => 'member.quote.get.price']);
	});
});
