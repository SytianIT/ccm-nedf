<?php

namespace App\Http\Middleware;

use App\SiteContent;
use Closure;
use Illuminate\Support\Facades\Auth;

class SystemStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $contents = SiteContent::findOrFail(1);
        
        if(!$contents->is_active && !Auth::user()->isSuperAdmin()){
            return redirect()->route('under.maintenance');
        }

        return $next($request);
    }
}
