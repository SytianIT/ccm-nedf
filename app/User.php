<?php

namespace App;

use App\BaseModel;
use App\Products\PriceList;
use App\Products\ProductPrice;
use App\Role;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\Auth;

class User extends BaseModel implements AuthenticatableContract,
                                        AuthorizableContract,
                                        CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'birthdate', 'deleted_at', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'other_data' => 'array'
    ];

    /**
     * User Eloquent Relations
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * member assigned pricelist
     * @return object
     */
    public function priceList()
    {
        return $this->belongsTo(PriceList::class, 'price_list_id');
    }

    /**
     * member assigned prices
     * @return object
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class, 'user_id');
    }

    /**
     * Scope Queries
     */
    public function scopeAdmins($query)
    {
        return $this->whereHas('roles', function($q){
                    $q->whereHas('permissions', function($q2){
                            $q2->where('name', '=', 'admin');
                        });
                });
    }

    public function scopeMembers($query)
    {
        return $this->whereHas('roles', function($q){
                    $q->whereHas('permissions', function($q2){
                            $q2->where('name', '=', 'member');
                        });
                });
    }

    /**
     * Setters * Getters
     */

    /**
     * Custom Functions
     */
    /**
     * Function to call to check if the authenticated user is admin
     * @return boolean [description]
     */         
    public function isSuperAdmin()
    {
        if($this->roles->contains(1)){
            return true;
        }

        return false;
    }

    /**
     * Function to call to check if the authenticated user is admin
     * @return boolean [description]
     */         
    public function isAdmin()
    {
        if($this->hasPermission('admin')){
            return true;
        }

        return false;
    }

    /**
     * Function to cal to check if the authenticated user is member only.
     * @return boolean [description]
     */
    public function isMember()
    {
        if($this->hasPermission('member')){
            return true;
        }

        return false;
    }

    /**
     * Method to check if the permission is permitted to this user role
     * @param  String  $permissionName
     * @return boolean 
     */
    public function hasPermission($permissionName)
    {
        $permitted = false;
        $permission = Permission::where('name', '=', $permissionName)->first();
        $assignedRole = $this->roles()->get();

        if($permission){
            $assignedRole->each(function ($item, $val) use (&$permitted, $permission){
                if($item->permissions->contains($permission)){
                    $permitted = true;
                    return false;
                }
            });
        }

        return $permitted;
    }
}
