<?php

namespace App;

use App\BaseModel;

class SiteContent extends BaseModel
{
    protected $table = 'site_contents';
}
