<html>
	<head>
		<meta charset="UTF-8">
		<title>North Eastern Door Frames - Account Changes.</title>
	</head>
	<body style="font-size:14px; font-family:sans-serif;margin:0 auto; width:605px;background:#ececec;padding:15px;">
		<div style="line-height: 0;width:570px;margin-bottom: 10px;">
			<img src="{{ asset('images/logo-large.png') }}" style="width: 100%">
		</div>
		<div style="background:#FFF;max-width:510px;padding:30px;margin-bottom: 10px;">
			<p style="line-height: 1.5em;">Hi {{ $member->firstname }},</p>
			<br>
			<p style="line-height: 1.5em;">This is to inform you that we made some changes in your account details.</p>
			<br>
			<p style="line-height: 1.5em;">Note: Please change your password immediately, <a href="{{ route('show.login') }}">Check my account!</a></p>
		</div>
	</body>
</html>

