@extends('layouts.app')

@section('css')
<style>
    html,body {
        height: 100%;
        background: #fff;
    }
</style>
@stop

@section('guest')
<div class="center-vertical">
    <div class="center-content">
        
        {!! Form::open(['route' => 'auth.login', 'method' => 'POST', 'id' => 'login-validation', 'class' => 'col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin']) !!}
            <img src="{{ asset('images/logo-large.png') }}" alt="" class="pad25B img-responsive">
            <div id="login-form" class="content-box bg-default">
                <div class="content-box-wrapper pad20A">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-user"></i>
                            </span>
                            <input type="username" name="username" id="login-username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}"  placeholder="Enter username">
                            @if ($errors->has('username'))
                            <label class="font-red font-size-11">{{ $errors->first('username') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" id="password" placeholder="Password">
                            @if ($errors->has('password'))
                            <label class="font-red font-size-11">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-blue-alt">Login</button>
                    </div>
                    <div class="row">
                        <div class="text-right col-md-12">
                            <a href="{{ route('show.password.reset') }}" title="Recover password">Forgot your password?</a>
                        </div>
                    </div>
                </div>
            </div>

        {!! Form::close() !!}

    </div>
</div>
@stop
