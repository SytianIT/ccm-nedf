@extends('layouts.app')

@section('css')
<style>
    html,body {
        height: 100%;
        background: #fff;
    }
</style>
@stop

@section('guest')
<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin">

            <img src="{{ asset('images/logo-large.png') }}" alt="" class="pad25B img-responsive">
            <div id="login-form" class="content-box bg-default">
                <div class="content-box-wrapper pad20A">
                    @if(session()->has('error'))
                    <div class="alert alert-danger mrg15B">
                        {{ session('error') }}
                    </div>
                    @endif
                    @if ($errors->has('token'))
                    <div class="alert alert-danger mrg15B">
                        {{ $errors->first('token') }}
                    </div>
                    @endif

                    {{ Form::open(['route' => 'password.reset', 'method' => 'POST', 'class' => 'form-horizontal']) }}
                        <div class="form-group">
                            <label for="email" class="col-md-12">E-Mail Address</label>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control {{ $errors->has('email') ? 'parsley-error' : '' }}" name="email" value="{{ $email or old('email') }}">
                                @if ($errors->has('email'))
                                    <label class="font-red font-size-11">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-12">Password</label>
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? 'parsley-error' : '' }}" name="password">
                                @if ($errors->has('password'))
                                    <label class="font-red font-size-11">{{ $errors->first('password') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-12">Confirm Password</label>
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control {{ $errors->has('password_confirmation') ? 'parsley-error' : '' }}" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <label class="font-red font-size-11">{{ $errors->first('password_confirmation') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-blue-alt">
                                    <i class="fa fa-btn fa-refresh"></i> Reset Password
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="token" value="{{ $token }}">
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
