@extends('layouts.app')

@section('css')
<style>
    html,body {
        height: 100%;
        background: #fff;
    }
</style>
@stop

<!-- Main Content -->
@section('guest')
<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin">

            <img src="{{ asset('images/logo-large.png') }}" alt="" class="pad25B img-responsive">
            <div id="login-form" class="content-box bg-default">
                <div class="content-box-wrapper pad20A">
                    @if (session('status'))
                        <div class="alert alert-success mrg15B">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    {!! Form::open(['route' => 'send.reset.link', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                        <div class="form-group{{ $errors->has('email') ? 'parsley-error' : '' }}">
                            <label for="email" class="col-md-12">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <label class="font-red font-size-11">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-block btn-blue-alt">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3 text-right">
                                <a href="{{ route('auth.login') }}"><i class="glyph-icon icon-angle-left"></i>&nbsp;BACK</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
