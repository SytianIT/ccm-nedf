<?php $noSideBar = true; ?>
@extends('layouts.app')

@section('css')
<style type="text/css">
    html,body {
        height: 100%;
    }
    body {
        background: #fff;
        overflow: hidden;
    }

</style>
@stop

@section('guest')
<img src="{{ asset('images/delight/image-resources/blurred-bg/blurred-bg-7.jpg') }}" class="login-img wow fadeIn" alt="">
<div class="center-vertical">
    <div class="center-content">

        <div class="col-md-6 center-margin">
            <div class="server-message wow bounceInDown inverse">
                {{-- <h1>Page Under Maintenance</h1> --}}
                <h2>Page Under Maintenance</h2>
                <p>We aim to give you the very best sevice we can that's why our technician is doing his best to make it happen. We'll be back before you know it, so please try again soon. Thanks for your patience!</p>
            </div>
        </div>

    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    /* WOW animations */

    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>
@stop