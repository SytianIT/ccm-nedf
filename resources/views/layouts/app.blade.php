<!DOCTYPE html> 
<html ng-app="syionApp">
<head>
    <meta charset="UTF-8">
    <title>@yield('title', 'North Eastern Door Frames')</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="base-url" content="{{ url('/') }}">
    <meta name="description" content="{{ isset($description) ? $description : 'no description' }}">
    <meta name="keywords" content="{{ isset($keywords) ? $keywords : 'no keywords' }}">
    
    @section('meta')
    {{-- Section for other Meta --}}
    @show

    {{-- Favicons --}}
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/favicon/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/favicon/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/favicon/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/favicon/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon/favicon.png') }}">

    {{-- Import Delight Admin Template CSS --}}
    {{-- <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"> --}}
    <link href="{{ asset('css/delight.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.2.1/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-punk.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    @section('css')
    @show
    
    {{-- Section for importing scripts that needs to loaded first --}}
    @section('head-scripts')
    @show

</head>
<body>
    <div id="loading">
        <div class="svg-icon-loader">
            <img src="{{ asset('/images/svg-loaders/bars.svg') }}" width="40" alt="">
        </div>
    </div>

    {{-- Layout Intended for Guest --}}
    @section('guest')
    @show

    {{-- Layout Intended for Admin --}}
    @if(Auth::user() && !isset($noSideBar))
    <div id="page-wrapper">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        </div>
        <aside id="page-sidebar">
            <div id="header-logo" class="logo-bg">
                <a href="" class="logo-content-big" title="DelightUI" style="background: url({{ asset('images/logo.png') }}) left 50% no-repeat; background-size: contain;">
                    Delight <i>UI</i>
                    <span>Material Design Dashboard Template</span>
                </a>
                <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-outdent"></i>
                </a>
            </div>
            <div class="scroll-sidebar">
                @include('layouts.inc.main-sidebar')
            </div>
        </aside>
        <main id="page-content-wrapper">
            <div id="page-content">
                <header id="page-header"> 
                    <div id="header-nav-left" class="pad15R">
                        <div class="user-account-btn dropdown">
                            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                                <img width="28" src="{{ $authUser->profile_img != null ? asset($authUser->profile_img) : asset('images/gravatar.jpg') }}" alt="Profile image">
                                <span>{{ $authUser->username }}</span>
                                <i class="glyph-icon icon-angle-down"></i>
                            </a>
                            <div class="dropdown-menu float-right">
                                <div class="box-sm">
                                    <div class="login-box clearfix">
                                        <div class="user-img">
                                            <a href="{{ $authUser->isAdmin() ? route('admin.profile') : route('member.profile') }}" title="" class="change-img">Change photo</a>
                                            <img src="{{ $authUser->profile_img != null ? asset($authUser->profile_img) : asset('images/gravatar.jpg') }}" alt="">
                                        </div>
                                        <div class="user-info">
                                            <a href="{{ $authUser->isAdmin() ? route('admin.profile') : route('member.profile') }}"><span>{{ $authUser->firstname }}</span></a>
                                            {{-- <a href="#" title="Edit profile">Edit profile</a> --}}
                                            {{-- <a href="#" title="View notifications">View notifications</a> --}}
                                        </div>
                                    </div>
                                    <div class="button-pane button-pane-alt pad5L pad5R text-center">
                                        <a href="{{ route('auth.logout') }}" class="btn btn-flat display-block font-normal btn-success">
                                            <i class="glyph-icon icon-power-off"></i>
                                            Logout
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- #header-nav-left -->

                    <div id="header-nav-right">

                    </div><!-- #header-nav-right -->
                    @section('header')
                    @show
                </header>
                <section id="page-wrapper">
                    <div id="page-title" class="pad0T">
                        <ul class="breadcrumb">
                            @section('crumbs')
                            @show
                        </ul>
                        <h2>@yield('page-heading', 'Page Title Here')</h2>
                        @section('sub-heading')
                        @show
                    </div>
                    @section('main')
                    @show
                </section>
            </div>
        </main>
    </div>
    @include('layouts.inc.notification')
    @endif
    <script src="{{ asset('js/delight.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.2.1/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
    <script src="{{ asset('js/all.js') }}"></script>
    @section('scripts')
    @show
</body>
</html>
