<div class="modal fade modal-frame-profile" id="modal-frame-profile-pictures">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Choose Frame Profile</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php $ctr = 1; ?>
                    @foreach($frameProfiles as $frame)
                    <div class="col-xs-12 col-sm-3 mrg15B">
                        <div class="thumbnail-box">
                            <div class="thumb-content">
                                <div class="center-vertical">
                                    <div class="center-content">
                                        <div class="thumb-btn animated zoomIn">
                                            <a href="javascript:;" class="btn btn-lg btn-round btn-success frame_profile_pic_chosen" title="Image Sample {{ $ctr }}" data-target=".img_src_{{ $ctr }}"><i class="glyph-icon icon-check"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="thumb-overlay bg-blue-alt"></div>
                            <img class="img_src_{{ $ctr }}" src="{{ asset($frame->path) }}" alt="{{ $frame->title }}" data-id="{{ $frame->id }}">
                        </div>
                    </div>
                    <?php $ctr++; ?>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue-alt" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>