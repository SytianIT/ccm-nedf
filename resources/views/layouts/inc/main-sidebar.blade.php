<ul id="sidebar-menu">
    @if($authUser->isAdmin())
        <li class="header"><span>Overview</span></li>
        <li>
            <a href="{{ route('admin') }}" title="Admin Dashboard">
                <i class="glyph-icon icon-linecons-tv"></i>
                <span>Dashboard</span>
            </a>
        </li>
         <li class="header"><span>Components</span></li>
        <li>
            <a href="javascript:void(0);" title="Quotes">
                <i class="glyph-icon icon-file-text"></i>
                <span>Quotes</span>
            </a>
        </li>
        @if($authUser->hasPermission('view_members'))
        <li>
            <a href="javascript:void(0);" title="Members">
                <i class="glyph-icon icon-group"></i>
                <span>Members</span>
            </a>
            <div class="sidebar-submenu">

                <ul>
                    @if($authUser->hasPermission('view_members'))
                    <li><a href="{{ route('members') }}" title="Members"><span>Members</span></a></li>
                    @endif
                    @if($authUser->hasPermission('create_member'))
                    <li><a href="{{ route('member.create') }}" title="New Member"><span>New</span></a></li>
                    @endif
                </ul>

            </div><!-- .sidebar-submenu -->
        </li>
        @endif
        <li>
            @if($authUser->hasPermission('view_base_pricelists'))
            <a href="" title="Price Lists">
                <i class="glyph-icon icon-money"></i>
                <span>Products / Price Lists</span>
            </a>
            <div class="sidebar-submenu">

                <ul>
                    <li><a href="{{ route('admin.price-lists') }}" title="Product Price List"><span>Product Prices</span></a></li>
                    <li><a href="{{ route('admin.price-lists.combinations') }}" title="Combination Price Lists"><span>Combination Prices</span></a></li>
                    <li><a href="{{ route('admin.frame-profiles') }}" title="Frame Pictures"><span>Frame Pictures</span></a></li>
                </ul>

            </div>
            @endif
        </li>
        @if($authUser->hasPermission('access_site_settings'))
        <li>
            <a href="javascript:void(0);" title="Options">
                <i class="glyph-icon icon-cogs"></i>
                <span>Options</span>
            </a>
            <div class="sidebar-submenu">

                <ul>
                    @if($authUser->hasPermission('access_site_settings'))
                    <li><a href="{{ route('admin.settings') }}" title="Site Settings"><span>Site Settings</span></a></li>
                    @endif
                    @if($authUser->hasPermission('view_admins'))
                    <li><a href="{{ route('users') }}" title="Admins"><span>Admins</span></a></li>
                    @endif
                    @if($authUser->hasPermission('view_roles'))
                    <li><a href="{{ route('roles') }}" title="Roles"><span>Roles</span></a></li>
                    @endif
                    @if($authUser->hasPermission('view_logs'))
                    <li><a href="{{ route('admin.audit-logs') }}" title="Roles"><span>Audit Logs</span></a></li>
                    @endif
                </ul>

            </div><!-- .sidebar-submenu -->
        </li>
        @endif
    @elseif($authUser->isMember())
    <li class="header"><span>Overview</span></li>
    <li>
        <a href="{{ route('member') }}" title="Member Dashboard">
            <i class="glyph-icon icon-linecons-tv"></i>
            <span>Dashboard</span>
        </a>
    </li>
     <li class="header"><span>Components</span></li>
    <li>
        <a href="{{ route('member.quotes') }}" title="Quotes">
            <i class="glyph-icon icon-file-text"></i>
            <span>Quotes</span>
        </a>
    </li>
    @endif
</ul><!-- #sidebar-menu -->