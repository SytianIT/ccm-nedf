@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('member') }}">Dashboard</a></li>
<li class="active">Quotes</li>
@stop

@section('page-heading', 'Quotes')

@section('main')
<div class="panel">
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
				<tr>
					<th width="15%">Date</th>
					<th width="15%">Quote No.</th>
					<th width="25%">Description</th>
					<th width="15%">$ Value</th>
					<th width="15%">Status</th>
					<th width="15%" class="text-right">Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<div class="text-center clearfix">
			<a href="{{ route('member.quote.create') }}" class="btn btn-blue-alt btn-md pull-left">NEW QUOTE</a>
		</div>
	</div>
</div>
@stop