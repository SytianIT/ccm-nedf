@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('member') }}">Dashboard</a></li>
<li class="active">Create Quote</li>
@stop

@section('page-heading', 'Create Quote')

@section('main')
<div class="panel">
	<div class="panel-body">
		{!! Form::open(['class' => 'form form-horizontal quote-creation-form', 'id' => 'quote-form']) !!}
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Quotation #</label>
					<p class="col-xs-12 col-sm-10 form-control-static font-bold">{{ $quoteNum }}</p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Customer Name:</label>
					<p class="col-xs-12 col-sm-10 form-control-static font-bold">{{ $authUser->firstname }}</p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Date:</label>
					<p class="col-xs-12 col-sm-10 form-control-static font-bold">{{ Carbon\Carbon::now()->toFormattedDateString() }}</p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Customer P/O #:</label>
					<p class="col-xs-12 col-sm-6"><input type="text" class="form-control" name="customer_po" maxlength="40"></p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Project:</label>
					<p class="col-xs-12 col-sm-6"><input type="text" class="form-control" name="project" maxlength="40"></p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Address:</label>
					<p class="col-xs-12 col-sm-6"><textarea name="address" id="address" rows="3" class="form-control" resize="false" style="resize:none"></textarea></p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Delivery Date:</label>
					<p class="col-xs-12 col-sm-6"><input type="text" class="form-control datepicker" name="delivery_date"></p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Contact:</label>
					<p class="col-xs-12 col-sm-6"><input type="text" class="form-control datepicker" name="contact"></p>
				</div>
				<div class="row mrg5B">
					<label class="col-xs-12 col-sm-2 control-label">Contact Phone:</label>
					<p class="col-xs-12 col-sm-6"><input type="text" class="form-control datepicker" name="contact_phone"></p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="row mrg5B text-right">
					<label class="col-xs-12 col-sm-12 control-label">Dynamic Profile Pic:</label>
					<img src="" alt="" class="frame_profile_preview frame-profile-preview img-responsive">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 mrg15T">
				<br>
				<div class="quote-table">
					<a href="javascript:;" class="btn btn-blue-alt btn-sm btn-copy-rows mrg10B">COPY</a>
					<a href="javascript:;" class="btn btn-danger btn-sm btn-delete-rows mrg10B">DELETE</a>
					<table id="quote-table" class="table table-responsive table-bordered">
						<thead>
							<tr>
								<th>0</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5 - 10</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th><th>32</th><th>33</th><th>34</th><th>35</th>
							</tr>
							<tr>
								<th colspan="2"></th><th colspan="29" class='text-center'>FRAME PREFERENCE</th><th colspan="4" class='text-center'>DOOR PREFERENCE</th><th colspan="2"></th>
							</tr>
							<tr class="quote-table-main-headings">
								<th>&nbsp;</th>
								<th>F / D / F&amp;D</th>
								<th>Door Ref</th>
								<th>Location</th>
								<th>Quantity</th>
								<th>Door or Reveal</th>
								<th>Door Height</th>
								<th>Door Width</th>
								<th>Door Thick</th>
								<th>Reveal Height</th>
								<th>Reveal Width</th>
								<th>Door Thick / Rebate Size</th>
								<th>Overall Height</th>
								<th>Overall Width</th>
								<th>Hand LH / RH</th>
								<th>Gauge &amp; Material</th>
								<th>Frame Profile</th>
								<th>Storm Mould</th>
								<th>Wall Construction</th>
								<th>Frame Fixing</th>
								<th>Wall Finishes</th>
								<th>Hinge Type</th>
								<th>Hinge Size</th>
								<th>Hinge Qty</th>
								<th>Frame Throat</th>
								<th>Hinge Fixing</th>
								<th>Lock Type</th>
								<th>Lock Height &amp; Striker</th>
								<th>Closer</th>
								<th>K.D / Welded</th>
								<th>Priming / Powdercoating</th>
								<th>Door Type</th>
								<th>Door Height</th>
								<th>Door Width</th>
								<th>Door Thick</th>
								<th>Special Notes</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
							<tr id="row-1" class="upper-most-row quote-rows" data-line="1">
								<td class="quote-col col-0 text-center">
									<div class="checkbox mrg0A">
	                                    <input type="checkbox" name="checkbox[]" data-line="1" data-target="#row-1" class="rows_check float-none mrg0A">
	                                </div>
								</td>
								{{-- COLUMN 1 --}}
								<td class="quote-col col-1" data-message="For this line select if you are pricing (F) frames only, (D) doors only, (F&D) frames and doors. This will activate only those columns that require data." data-require-message="">
									<select name="doors_frames_selection[1]" class="form-control doors_frames_selection">
										<option value="">-Select-</option>
										@foreach($col1 as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 2 --}}
								<td class="quote-col col-2 door-only-col"  data-message="Enter the Door Reference Number from the plan" data-require-message="">
									<input type="text" name="door_reference[1]" title="door_reference" class="form-control" maxlength="10">
								</td>
								{{-- COLUMN 3 --}}
								<td class="quote-col col-3 frame-only-col" data-message="Enter the location of the frames/doors as per the plans" data-require-message="">
									<input type="text" name="location[1]" title="location" class="form-control" maxlength="40">
								</td>
								{{-- COLUMN 4 --}}
								<td class="quote-col col-4 frame-only-col" data-message="Enter the quantity required" data-require-message="">
									<input type="text" name="quantity[1]" title="quantity" class="form-control input-mask" data-inputmask="'mask':'999'">
								</td>
								{{-- COLUMN 5 - A --}}
								<td class="quote-col col-5-a frame-only-col" data-message="Select ‘Door Size’ to enter door sizes and have frame sizes automatically calculated / Select ‘Reveal Size’ to enter the frame reveal size instead">
									<select name="door_thick_or_rebate_size[1]" id="door_rebate_select" class="form-control door_rebate_select door_thick_or_rebate_size">
										<option value="">-Select-</option>
										@foreach($colFiveTen as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 5 --}}
								<td class="quote-col col-5 frame-only-col col-door-rebate-drop col-door-rebate-disable col-door-choice" data-message="Select the door height or choose Custom to enter your own">
									<select name="door_height_first[1]" class="form-control select-with-custom door_height_first">
										<option value="">-Select-</option>
										@foreach($products->door_height as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" class="mrg5T form-control hidden select-custom-field door_height_first_custom" name="door_height_first_custom[1]" placeholder="Enter Value...">
								</td>
								{{-- COLUMN 6 --}}
								<td class="quote-col col-6 frame-only-col col-door-rebate-drop col-door-rebate-disable col-door-choice" data-message="Select the door width or choose Custom to enter your own">
									<select name="door_width_first[1]" class="form-control select-with-custom door_width_first">
										<option value="">-Select-</option>
										@foreach($products->door_width as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" class="mrg5T form-control hidden select-custom-field door_width_first_custom" name="door_width_first_custom[1]" placeholder="Enter Value...">
								</td>
								{{-- COLUMN 7 --}}
								<td class="quote-col col-7 frame-only-col col-door-rebate-drop col-door-rebate-disable col-door-choice" data-message="Select the door thickness or choose Custom to enter your own">
									<select name="door_thick_first[1]" class="form-control select-with-custom door_thick_first">
										<option value="">-Select-</option>
										@foreach($products->door_thick as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" class="mrg5T form-control hidden select-custom-field door_thick_first_custom" name="door_thick_first_custom[1]" placeholder="Enter Value...">
								</td>
								{{-- COLUMN 8 --}}
								<td class="quote-col col-8 frame-only-col col-door-rebate-drop col-door-rebate-disable col-reveal-choice" data-message="Select the frame reveal height or choose Custom to enter your own">
									<select name="reveal_height[1]" class="reveal_height_selection_col_8 form-control select-with-custom reveal_height">
										<option value="">-Select-</option>
										@foreach($products->reveal_height as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" class="mrg5T form-control hidden select-custom-field reveal_height_selection_col_8_custom" name="reveal_height_custom[1]" placeholder="Enter Value...">
								</td>
								{{-- COLUMN 9 --}}
								<td class="quote-col col-9 frame-only-col col-door-rebate-drop col-door-rebate-disable col-reveal-choice" data-message="Select the frame reveal width or choose Custom to enter your own">
									<select name="reveal_width[1]" class="reveal_width_selection_col_9 form-control select-with-custom reveal_width">
										<option value="">-Select-</option>
										@foreach($products->reveal_width as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" class="mrg5T form-control hidden select-custom-field reveal_width_selection_col_9_custom" name="reveal_width_custom[1]" placeholder="Enter Value...">
								</td>
								{{-- COLUMN 10 --}}
								<td class="quote-col col-10 frame-only-col col-door-rebate-drop col-door-rebate-disable col-reveal-choice" data-message="Select to enter either the Door Thickness or Rebate Size">
									<div class="mrg10B">
										<?php $ctr = 0; ?>
										@foreach($colTen as $key => $item)
										<label class="radio-inline">
											<input type="radio" id="reveal_{{ $key }}" name="reveal_radio_option[1]" value="{{ $key }}" class="reveal_radio_option" {{ $ctr == 0 ? 'checked' : '' }}>
											{{ $item }}
										</label>
										<?php $ctr++; ?>
										@endforeach
									</div>
									<select name="reveal_door_thick_drop[1]" class="form-control reveal-drop-result reveal_door_thick reveal_door_thick_drop">
										@foreach($products->door_thick as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
									<select name="reveal_rebate_size_drop[1]" class="form-control reveal-drop-result reveal_rebate_size hidden reveal_rebate_size_drop select-with-custom">
										@foreach($products->rebate_size as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" class="mrg5T form-control hidden select-custom-field reveal_rebate_size_drop_custom" name="reveal_rebate_size_drop_custom[1]" placeholder="Enter Value...">
								</td>
								{{-- COLUMN 11 --}}
								<td class="quote-col col-11 frame-only-col">
									<p class="overall_height_display font-bold text-center">0</p>
									<input type="hidden" name="overall_height[1]" title="Overall Height" class="overall_height">
								</td>
								{{-- COLUMN 12 --}}
								<td class="quote-col col-12 frame-only-col">
									<p class="overall_width_display font-bold text-center">0</p>
									<input type="hidden" name="overall_width[1]" title="Overall Width" class="overall_width">
								</td>
								{{-- COLUMN 13 --}}
								<td class="quote-col col-13 frame-only-col" data-message="Select either Left Hand, Right Hand, Double or Fire Rated Double">
									<select name="hand_type[1]" class="form-control hand_type">
										<option value="">-Select-</option>
										@foreach($products->hand_form as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 14 --}}
								<td class="quote-col col-14 frame-only-col" data-message="Select closest option and use Special Notes column to advise of any queries">
									<select name="gauge_material[1]" class="form-control gauge_material">
										<option value="">-Select-</option>
										@foreach($products->gauge_material as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 15 --}}
								<td class="quote-col col-15 frame-only-col text-center">
									<a href="javascript:;" class="btn btn-blue-alt btn-xs choose_frame_profile_pic" data-toggle="modal" data-target="#modal-frame-profile-pictures" data-selected-image-path="" data-selected-image-class="">CHOOSE IMAGE</a>
									<input type="hidden" name="frame_profile[1]" class="form-control frame_profile">
								</td>
								{{-- COLUMN 16 --}}
								<td class="quote-col col-16 frame-only-col" data-message="Select if Storm Mould required.">
									<select name="storm_mould[1]" class="form-control storm_mould">
										@foreach($products->mould as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 17 --}}
								<td class="quote-col col-17 frame-only-col" data-message="Enter Door Side / Stud Size / Off Side.">
									<input type="text" class="form-control wall_construction input-mask" name="wall_construction[1]" data-inputmask="'mask':'999/999/999'">
								</td>
								{{-- COLUMN 18 --}}
								<td class="quote-col col-18 frame-only-col" data-message="If unsure use Special Notes column to advise of any queries.">
									<select name="frame_fixing[1]" class="frame_fixing form-control frame_fixing">
										<option value="">-Select-</option>
										@foreach($products->frame_fixing as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 19 --}}
								<td class="quote-col col-19 frame-only-col" data-message="If unsure use Special Notes column to advise of any queries.">
									<select name="wall_finishes[1]" class="wall_finishes form-control wall_finishes">
										<option value="">-Select-</option>
										@foreach($products->wall_finishes as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 20 --}}
								<td class="quote-col col-20 frame-only-col" data-message="If unsure use Special Notes column to advise of any queries.">
									<select name="hinge_type[1]" class="form-control hinge_type">
										<option value="">-Select-</option>
										@foreach($products->hinge_type as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 21 --}}
								<td class="quote-col col-21 frame-only-col" data-message="Use the Custom field to enter Pivot Pin Code if selected in Hinge Type.">
									<select name="hinge_size[1]" class="form-control hinge_size">
										<option value="">-Select-</option>
										@foreach($products->hinge_size as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 22 --}}
								<td class="quote-col col-22 frame-only-col" data-message="Calculated number can be overridden if required.">
									<input type="text" class="form-control hinge_qty_input" name="hinge_qty[1]">
								</td>
								{{-- COLUMN 23 --}}
								<td class="quote-col col-23 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<input type="text" class="form-control frame_throat_size" name="frame_throat_size[1]" data-inputmask="'mask':'999'">
								</td>
								{{-- COLUMN 24 --}}
								<td class="quote-col col-24 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="hinge_fixing[1]" class="form-control hinge_fixing">
										<option value="">-Select-</option>
										@foreach($products->hinge_fixing as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 25 --}}
								<td class="quote-col col-25 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="lock_type[1]" class="lock_type form-control lock_type">
										<option value="">-Select-</option>
										@foreach($products->lock_type as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
									<input type="text" name="lock_type_electric[1]" class="mrg5T hidden form-control lock_type_electric input-mask" data-inputmask="'mask':'********'">
								</td>
								{{-- COLUMN 26 --}}
								<td class="quote-col col-26 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="lock_height_striker[1]" class="form-control select-with-custom lock_height_striker">
										<option value="">-Select-</option>
										@foreach($products->lock_height_striker as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" name="lock_height_striker_custom[1]" class="mrg5T hidden form-control input-mask select-custom-field" maxlength="20">
								</td>
								{{-- COLUMN 27 --}}
								<td class="quote-col col-27 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="closer[1]" class="form-control">
										@foreach($products->closer as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 28 --}}
								<td class="quote-col col-28 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="kd_welded[1]" class="form-control">
										<option value="">-Select-</option>
										@foreach($products->welded as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 29 --}}
								<td class="quote-col col-29 frame-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="priming_powder_coating[1]" class="form-control">
										<option value="">-Select-</option>
										@foreach($products->priming_powder_coating as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
									</select>
								</td>
								{{-- COLUMN 30 --}}
								<td class="quote-col col-30 door-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="door_type_second[1]" class="form-control select-with-custom door_type_second">
										<option value="">-Select-</option>
										@foreach($products->door_type as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" name="door_type_second_custom[1]" class="mrg5T hidden door_type_second_custom select-custom-field form-control">
								</td>
								{{-- COLUMN 31 --}}
								<td class="quote-col col-31 door-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="door_height_second[1]" class="form-control select-with-custom door_height_second">
										<option value="">-Select-</option>
										@foreach($products->door_height as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" name="door_height_second_custom[1]" class="mrg5T hidden door_height_second_custom select-custom-field form-control">
								</td>
								{{-- COLUMN 32 --}}
								<td class="quote-col col-32 door-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="door_width_second[1]" class="form-control select-with-custom door_width_second">
										<option value="">-Select-</option>
										@foreach($products->door_width as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" name="door_width_second_custom[1]" class="mrg5T hidden door_width_second_custom select-custom-field form-control">
								</td>
								{{-- COLUMN 33 --}}
								<td class="quote-col col-33 door-only-col" data-message="Use the Special Notes column to advise of any queries.">
									<select name="door_thick_second[1]" class="form-control select-with-custom door_thick_second">
										<option value="">-Select-</option>
										@foreach($products->door_thick as $key => $item)
										<option value="{{ $key }}">{{ $item }}</option>
										@endforeach
										<option value="custom">Custom</option>
									</select>
									<input type="text" name="door_thick_second_custom[1]" class="mrg5T hidden door_thick_second_custom select-custom-field form-control">
								</td>
								{{-- COLUMN 34 --}}
								<td class="quote-col both-col col-34" data-message="Use the Special Notes column to advise of any queries.">
									<textarea name="special_notes[1]" class="special_notes form-control" rows="3"></textarea>
								</td>
								{{-- COLUMN 35 --}}
								<td class="quote-col both-col col-35 text-center">
									<a href="javascript:;" class="btn btn-blue-alt btn-generate-price btn-xs btn-block" data-counter="1" data-price-calculate="{{ route('member.quote.get.price') }}">GET PRICE</a>
									<p class="mrg15T price-calculation font-bold font-size-16">0.00</p>
									<div class="remove-border glyph-icon icon-spin-4 icon-spin icon-price-preloader hidden" title="Loading..."></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@include('layouts.inc.modal-quote-frame-profile')
@stop