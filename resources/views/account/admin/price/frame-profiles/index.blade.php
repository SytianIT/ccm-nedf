@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Frame Profile</li>
@stop

@section('page-heading', 'Frame Profile')

@section('main')
<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero clearfix">List of Frame Profiles @if($frames->count() < 16)<a href="{{ route('admin.frame-profile.create') }}" class="btn btn-blue-alt mrg10L">CREATE NEW</a>@endif</h3>
		<table class="table table-hover table-bordered">
			<thead>
				<th class="text-center" style="max-width: 250px;">Image</th>
				<th class="text-center" width="50%">Title</th>
				<th class="text-center" width="20%">Actions</th>
			</thead>
			<tbody>
				@forelse($frames as $frame)
				<tr>
					<td>@if($frame->path != null)<img src="{{ asset($frame->path) }}" alt="{{ $frame->title }}" class="img-responsive" >@else<span>n/a</span>@endif</td>
					<td>{{ $frame->title }}</td>
					<td class="text-center">
						<a href="{{ route('admin.frame-profile.edit', $frame->id) }}" class="btn btn-xs btn-success">EDIT</a>
						<a href="{{ route('admin.frame-profile.delete', $frame->id) }}" class="btn btn-xs btn-danger">DELETE</a>
					</td>
				</tr>
				@empty
				<tr><td colspan="3">No records found!</td></tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@stop