@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li><a href="{{ route('admin.frame-profiles') }}">Frame Profiles</a></li>
<li class="active">Create Frame Profiles</li>
@stop

@section('page-heading', 'Create Frame Profile')

@section('main')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			{!! Form::open(['route' => 'admin.frame-profile.store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal bordered-row']) !!}
			<div class="form-group">
                <label class="col-sm-3 control-label">Image</label>
                <div class="col-sm-6">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="max-width: 100%;"><img src="" class="img-responsive"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <input name="image" type="file" id="file-browse">
                            <div class="ripple-wrapper"></div></span>
                            <em class="font-size-11">For better results, please provide an image with a resolution of 1200 x 760.</em>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                    @if ($errors->has('image'))
                    <p class="font-red font-size-11">{{ $errors->first('image') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-6">
                    <input type="text" name="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ old('title') }}">
                    @if ($errors->has('title'))
                    <p class="font-red font-size-11">{{ $errors->first('title') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-6">
                    <textarea name="description" class="form-control {{ ($errors->has('description')) ? 'parsley-error' : '' }}" placeholder="Description">{{ old('description') }}</textarea>
                </div>
            </div>
            <div class="form-group">
            	<label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6">
                	<a href="{{ route('admin.frame-profiles') }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		$('.fileinput-new').on('click', function(){
			$('#file-browse').click();
		});

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.fileinput-preview').find('img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$('#file-browse').on('change', function(){
			readURL(this);
		});
	});
</script>
@stop