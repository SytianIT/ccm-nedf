@extends('layouts.app')

@section('page-heading', 'Combinations Price Lists')

@section('main')
<div class="panel">
	<div class="panel-body">
		{!! Form::open(['route' => 'admin.price-lists.combinations.update', 'method' => 'POST', 'id' => 'form-update-combination']) !!}
		<table class="table table-bordered table-pricelists table-hover table-combination-pricelist"> 
			<thead>
				<tr class="row-heading">
					<th colspan="6">Related Columns</th>
					<th colspan="4">Price List</th>
				</tr>
				<tr class="row-heading">
					<th width="2%">&nbsp;</th>
					<th width="10%">Column 11</th>
					<th width="10%">Column 12</th>
					<th width="12%">Column 13</th>
					<th width="12%">Column 14</th>
					<th width="10%">Column 23</th>
					@foreach($priceLists as $list)
					<th width="11%" class='text-right'>{{ $list->title }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				<?php $ctr = 1; ?>
				@forelse($combinations as $combination)
				<tr class="combination-row" data-counter="{{ $ctr }}">
					<td><a href="javascript:;" class="btn-remove-row btn btn-danger btn-xs" title="Delete Row"><i class="glyph-icon icon-close"></i></a></td>
					<td>
						<input type="hidden" name="combination[{{ $ctr }}][id]" data-name="id" value="{{ $combination->id }}">
						<input type="number" class="form-control" name="combination[{{ $ctr }}][col_11]" placeholder="Column 11 value..." data-name="col_11" value="{{ $combination->col_11 }}" required>
					</td>
					<td>
						<input type="number" class="form-control" name="combination[{{ $ctr }}][col_12]" placeholder="Column 12 value..." data-name="col_12" value="{{ $combination->col_12 }}" required>
					</td>
					<td>
						<select name="combination[{{ $ctr }}][col_13]" class="form-control" data-name="col_13" required>
							<option value="">Column 13 value...</option>
							@foreach($handChilds as $item)
							<option value="{{ $item->name }}" {{ $combination->col_13 == $item->name ? 'selected' : '' }}>{{ $item->title }}</option>
							@endforeach
						</select>
					</td>
					<td>
						<select name="combination[{{ $ctr }}][col_14]" class="form-control" data-name="col_14" required>
							<option value="">Column 14 value...</option>
							@foreach($gaugeChilds as $item)
							<option value="{{ $item->name }}" {{ $combination->col_14 == $item->name ? 'selected' : '' }}>{{ $item->title }}</option>
							@endforeach
						</select>
					</td>
					<td>
						<input type="number" class="form-control" name="combination[{{ $ctr }}][col_23]" placeholder="Column 23 value..." data-name="col_23" value="{{ $combination->col_23 }}" required>
					</td>
					@foreach($priceLists as $list)
					<td>
						<?php 
						$price = $combination->getPriceList($list->name);
						if($price != null){
							$displayedPrice = ($price->is_tba) ? 'TBA' : $price->base_price;
						} else {
							$displayedPrice = '';
						}
						?>
						<input type="text" name="combination[{{ $ctr }}][{{ $list->name }}]" class="text-right form-control input-price {{ $list->name }}" placeholder="{{ $list->title }} Price" data-price-name="{{ $list->name }}" data-name="{{ $list->name }}" value="{{ $displayedPrice }}">
					</td>
					@endforeach
				</tr>
				@empty
				<tr class="combination-row" data-counter="1">
					<td><a href="javascript:;" class="btn-remove-row btn btn-danger btn-xs" title="Delete Row"><i class="glyph-icon icon-close"></i></a></td>
					<td>
						<input type="hidden" name="combination[1][id]" data-name="id">
						<input type="number" class="form-control" name="combination[1][col_11]" placeholder="Column 11 value..." data-name="col_11" required>
					</td>
					<td>
						<input type="number" class="form-control" name="combination[1][col_12]" placeholder="Column 12 value..." data-name="col_12" required>
					</td>
					<td>
						<select name="combination[1][col_13]" class="form-control" data-name="col_13" required>
							<option value="">Column 13 value...</option>
							@foreach($handChilds as $item)
							<option value="{{ $item->name }}">{{ $item->title }}</option>
							@endforeach
						</select>
					</td>
					<td>
						<select name="combination[1][col_14]" class="form-control" data-name="col_14" required>
							<option value="">Column 14 value...</option>
							@foreach($gaugeChilds as $item)
							<option value="{{ $item->name }}">{{ $item->title }}</option>
							@endforeach
						</select>
					</td>
					<td>
						<input type="number" class="form-control" name="combination[1][col_23]" placeholder="Column 23 value..." data-name="col_23">
					</td>
					@foreach($priceLists as $list)
					<td>
						<input type="text" name="combination[1][{{ $list->name }}]" class="text-right form-control input-price {{ $list->name }}" placeholder="{{ $list->title }} Price" data-price-name="{{ $list->name }}" data-name="{{ $list->name }}">
					</td>
					@endforeach
				</tr>
				@endforelse
			</tbody>
		</table>
		<div class="mrg25T">
			<a href="javascript:;" id="add-combination-row" class="btn btn-info">ADD ROW</a>&nbsp;&nbsp;
			<button type="submit" class="btn btn-blue-alt">UPDATE PRICELIST</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		function updateThisElementName(element, counter)
		{
			var attrName = element.data('name');

			element.attr('name', 'combination'+'['+counter+']'+'['+attrName+']');
			element.val('');
		}

		function checkRemainingRows()
		{
			var currLength = $('.table-combination-pricelist tbody tr').length;

			if(currLength == 1){
				$('.table-combination-pricelist tbody').find('.btn-remove-row').addClass('hidden');
			} else {
				$('.table-combination-pricelist tbody').find('.btn-remove-row').removeClass('hidden');
			}
		}
		checkRemainingRows();

		$('#add-combination-row').on('click', function(){
			var lastElement = $('.table-combination-pricelist tbody tr:last-child'),
				counter = lastElement.data('counter'),
				newCounter = parseInt(counter) + 1,
				cloned = lastElement.clone();

			cloned.find('select').each(function(){
				updateThisElementName($(this), newCounter);
			});
			cloned.find('input').each(function(){
				updateThisElementName($(this), newCounter);
			});
			cloned.attr('data-counter', newCounter);
			$('.table-combination-pricelist tbody tr:last-child').after(cloned);
			checkRemainingRows();
		});

		$('body').on('click', '.btn-remove-row', function(){
			var element = $(this).closest('.combination-row');

			element.fadeOut(function(){
				$(this).remove();
				checkRemainingRows();
			});
		});
	});
</script>
@stop