@extends('layouts.app')

@section('page-heading', 'Price Lists')

@section('main')
<div class="panel">
	<div class="panel-body">
		@if($authUser->hasPermission('view_base_pricelists'))
		<div class="alert alert-warning mrg10B">
		    <div class="bg-orange alert-icon">
		        <i class="glyph-icon icon-warning"></i>
		    </div>
		    <div class="alert-content">
		    <h4 class="alert-title">Warning</h4>
		        <p>Products with no delete buttons are considered as the default and cannot be deleted, because deleting them may result in quotation columns logic error.</p>
		    </div>
		</div>
		{!! Form::open(['route' => 'admin.price-lists.update', 'method' => 'POST', 'id' => 'form-pricelists']) !!}
		<table class="table table-bordered table-hover table-pricelists">
			<thead>
				<thead>
					<tr class="row-heading">
						<th width="40%" colspan="2">Product</th>
						@foreach($priceLists as $list)
						<th width="15%" class="text-right text-uppercase">{{ $list->title }}</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					@forelse($products as $product)
					<tr class="row-parent">
						<td colspan="6" width="15%">{{ $product->title }}
							&nbsp;&nbsp;&nbsp;<a href="javascript:;" class="btn btn-success btn-xs btn-sub-item" data-target=".col-{{ $product->id }}">ADD SUBITEM</a>
						</td>
					</tr>
					@if($product->childs->count() > 0)
						<?php $ctr = 1; $total = $product->childs->count(); ?>
						@foreach($product->childs as $child)
						<tr class="row-child col-{{ $product->id }} {{ $total == $ctr ? 'last-subitem' : '' }}" data-counter="{{ $ctr }}" data-product-name="{{ $product->name }}">
							<td colspan="2">
								<a href="javascript:;" class="btn btn-danger btn-xs btn-remove-subitem {{ $child->default ? 'hidden' : '' }}"><i class="glyph-icon icon-close"></i></a>
								&nbsp;&nbsp;&nbsp;<i class="glyph-icon icon-angle-double-right"></i>&nbsp;&nbsp;&nbsp;
								<input type="text" name="{{ $product->name }}[{{ $ctr }}][{{ $child->name }}]" class="form-control sub-item-title" placeholder="Sub Item Title" value="{{ $child->title }}">
							</td>
							@foreach($priceLists as $list)
								<?php 
								$price = $child->getPriceList($list->name);
								if($price != null){
									$displayedPrice = ($price->is_tba) ? 'TBA' : $price->base_price;
								} else {
									$displayedPrice = '';
								}
								?>
								<td>
									<input type="text" name="{{ $product->name }}[{{ $ctr }}][{{ $list->name }}]" class="{{ ($product->name == 'welded') ? 'with-less' : '' }} form-control input-price {{ $list->name }}" placeholder="{{ $list->title }} Price" data-price-name="{{ $list->name }}" value="{{ $displayedPrice }}">
									@if($product->name == 'welded')
									<em class="font-size-11 font-red">( less .6% )</em>
									@endif
								</td>
							@endforeach
						</tr>
						<?php $ctr++; ?>
						@endforeach
					@endif
					@empty
					@endforelse
				</tbody>
			</thead>
		</table>
		@if($authUser->hasPermission('edit_base_pricelists'))
		<div class="mrg25T pad25B bordered-row">
			<div class="form-group">
				<div class="checkbox checkbox-info">
                    <label>
                        <input type="checkbox" name="update_members_price" id="update_members_price" class="custom-checkbox">
                        Update All Members Price <em class="small">( Check if you want to reflect price changes to all members custom prices )</em>
                    </label>
                </div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-blue-alt confirm-submit" data-form="#form-pricelists" data-confirm-text="You cannot revert this changes.">UPDATE PRICELIST</button>
			</div>
		</div>
		@endif
		{!! Form::close() !!}
		@else
		<h5>Lack of permission to make changes in site settings</h5>
		@endif
	</div>
</div>
@stop

@section('scripts')
<script>
	$('.input-price.with-less').on('change', function(){
		var $this = $(this),
			currVal = $this.val(),
			percentVal = 0.006 * parseInt(currVal),
			lessVal = currVal - percentVal;

		if($.isNumeric(currVal)){
			if($.isNumeric(percentVal)){
				if(percentVal > currVal){
					$this.val(0);
				} else {
					$this.val(lessVal);
				}
			} else {
				$this.val(currVal);
			}
		} else {
			$this.val(currVal);
		}
	});

	$('.input-price').on('change', function(){
    	var $val = $(this).val(),
    		$newVal = $val.replace(/[^0-9a-zA-Z.]/g , "");

    	$(this).val($newVal);
    });

    $('.btn-sub-item').on('click', function(){
    	var target = $(this).data('target'),
    		rows = $(target),
    		lastElement = $('.table-pricelists').find(target+'.last-subitem'),
    		cloned = lastElement.clone(),
    		latestCounter = lastElement.data('counter'),
    		newCounter = parseInt(latestCounter) + 1,
    		prodName = lastElement.data('product-name');

    	cloned.attr('data-counter', newCounter);
    	cloned.find('.sub-item-title').attr('name', prodName+"["+newCounter+"]"+"[undefined]");
    	cloned.find('.btn-remove-subitem').removeClass('hidden');
    	cloned.find('.input-price').val('');
    	cloned.find('.sub-item-title').val('');
    	cloned.find('.input-price').each(function(){
    		var listName = $(this).data('price-name');

    		$(this).attr('name', prodName+"["+newCounter+"]"+"["+listName+"]");
    	});
    	lastElement.removeClass('last-subitem');
    	lastElement.after(cloned);
    });

    $('body').on('click', '.btn-remove-subitem', function(){

    	$(this).closest('.row-child').fadeOut(function(){
    		var prevEl = $(this).prev('.row-child');

    		if($(this).hasClass('last-subitem')){
    			prevEl.addClass('last-subitem');
    		}
    		$(this).remove();
    	});
    });
</script>
@stop