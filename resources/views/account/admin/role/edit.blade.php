@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li><a href="{{ route('roles') }}">Roles</a></li>
<li class="active">Edit Role</li>
@stop

@section('page-heading', 'Edit Role')

@section('main')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			@if($authUser->hasPermission('update_role'))
			{!! Form::model($role, ['route' => ['role.update', $role->id], 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
			<div class="form-group">
				<label class="col-sm-3 control-label">Title <span class="req">*</span></label>
				<div class="col-sm-6">
					<input type="text" name="title" value="{{ $role->title ?: old('title') }}" placeholder="Role Title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}">
					@if ($errors->has('title'))
                    <label class="font-red font-size-11">{{ $errors->first('title') }}</p>
                    @endif
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Description</label>
				<div class="col-sm-6">
					{!! Form::textarea('description', null, ['class' => 'form-control textarea-no-resize', 'placeholder' => 'Description', 'rows' => 5]) !!}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Permissions</label>
				<div class="col-sm-6">
					<?php $countPermission = $permissions->count(); $division = round($countPermission / 2); $ctr = 1; ?>
					<div class="row">
						@foreach($permissions as $permission)
						@if($ctr == 1)
						<div class="col-xs-6">
							@endif
							<div class="checkbox checkbox-info mrg5B">
								<label>
									<input type="checkbox" id="{{ $permission->name }}" name="permissions[]" value="{{ $permission->id }}" class="custom-checkbox" {{ $role->permissions->contains($permission) ? 'checked' : '' }}>
									<strong>{{ $permission->title }}</strong>
								</label>
							</div>
							@if($ctr == $division)
						</div>
						<div class="col-xs-6">
							@endif
							<?php $ctr++; ?>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">&nbsp;</label>
				<div class="col-sm-6 text-right">
					<a href="{{ route('roles') }}" class="btn btn-sm btn-danger">CANCEL</a>
					<button type="submit" class="btn btn-success btn-sm">SAVE</button>
				</div>
			</div>
			{!! Form::close() !!}
			@else
			<h5>Lack of permission to update details of a role.</h5>
			@endif
		</div>
	</div>
</div>
@stop