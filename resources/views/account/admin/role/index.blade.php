@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Roles</li>
@stop

@section('page-heading', 'Roles')

@section('main')
<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero clearfix">List of Created Roles @if($authUser->hasPermission('create_role'))<a href="{{ route('role.create') }}" class="btn btn-blue-alt mrg10L">CREATE NEW</a>@endif</h3>
		@if($authUser->hasPermission('view_roles'))
		<table class="table table-hover">
			<thead>
				<tr>
					<th width="40%">Name</th><th width="30%">Users Assigned</th><th width="30%" class="text-right">Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($roles as $role)
				<tr>
					<td>{{ $role->title }}</td>
					<td>{{ $role->users->count() }}</td>
					<td class="text-right">
						@if($authUser->hasPermission('update_role'))
						<a href="{{ route('role.edit', $role->id) }}" class="btn btn-xs btn-success">EDIT</a>
						@endif
						@if($authUser->hasPermission('delete_role'))
						<a href="{{ route('role.delete', $role->id) }}" class="btn btn-xs btn-danger">DELETE</a>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="3">No roles found!</td>
				</tr>
				@endforelse
			</tbody>
		</table>	
		@else
		<h5>Lack of permission to view the list of created roles.</h5>
		@endif
	</div>
</div>
@stop