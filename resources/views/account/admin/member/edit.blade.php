@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li><a href="{{ route('members') }}">Members</a></li>
<li class="active">Edit Member</li>
@stop

@section('page-heading', 'Edit Member')

@section('main')
@include('account.admin.member.inc.sub-menu')
<div class="panel">
    <div class="panel-body">
        <div class="example-box-wrapper">
            @if($authUser->hasPermission('update_member'))
            {!! Form::model($member, ['route' => ['member.update', $member->id], 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">First Name <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="firstname" class="form-control {{ ($errors->has('firstname')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ $member->firstname ?: old('firstname') }}">
                    @if ($errors->has('firstname'))
                    <label class="font-red font-size-11">{{ $errors->first('firstname') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-6">
                    <input type="text" name="lastname" class="form-control {{ ($errors->has('lastname')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ $member->lastname ?: old('lastname') }}">
                    @if ($errors->has('lastname'))
                    <label class="font-red font-size-11">{{ $errors->first('lastname') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Address</label>
                <div class="col-sm-6">
                    <input type="text" name="address" class="form-control {{ ($errors->has('address')) ? 'parsley-error' : '' }}" placeholder="Address" value="{{ $member->address ?: old('address') }}">
                    @if ($errors->has('address'))
                    <label class="font-red font-size-11">{{ $errors->first('address') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Contact Number</label>
                <div class="col-sm-6">
                    <input type="text" name="contact_number" class="form-control {{ ($errors->has('contact_number')) ? 'parsley-error' : '' }}" placeholder="Contact Number" value="{{ $member->contact_number ?: old('contact_number') }}">
                    @if ($errors->has('contact_number'))
                    <label class="font-red font-size-11">{{ $errors->first('contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ $member->email ?: old('email') }}">
                    @if ($errors->has('email'))
                    <label class="font-red font-size-11">{{ $errors->first('email') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Username <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ $member->username ?: old('username') }}">
                    @if ($errors->has('username'))
                    <label class="font-red font-size-11">{{ $errors->first('username') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Password <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password" value="">
                    <p><em class="font-size-11">(To change password type in here the new password)</em></p>
                    @if ($errors->has('password'))
                    <label class="font-red font-size-11">{{ $errors->first('password') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Password Confirmation <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
                    @if ($errors->has('password_confirmation'))
                    <label class="font-red font-size-11">{{ $errors->first('password_confirmation') }}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6 checkbox checkbox-info mrg5B">
                    <label>
                        <input type="checkbox" id="notify-via-email" checked name="notify_merchant" class="custom-checkbox">
                        <strong>Notify member of changes via e-mail.</strong>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('members') }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">UPDATE</button>
                </div>
            </div>
            {!! Form::close() !!}
            @else
            <h5>Lack of permission to create new admin user.</h5>
            @endif
        </div>
    </div>
</div>
@stop