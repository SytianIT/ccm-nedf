<table class="table table-bordered table-hover table-pricelists">
	<thead>
		<tr>
			<th width="40%">&nbsp;</th>
			<th width="60%" colspan="2" class="text-center">{{ $selectedPriceList != null ? $selectedPriceList->title : 'n/a' }}</th>
		</tr>
		<tr class="row-heading">
			<th>Product</th>
			<th width="30%" class="text-center text-uppercase">Base Price</th>
			<th width="30%" class="text-center text-uppercase">Member Price</th>
		</tr>
	</thead>
	<tbody>
	@forelse($products as $product)
	<tr class="row-parent">
		<td colspan="6" width="15%">{{ $product->title }}</td>
	</tr>
	@if($product->childs->count() > 0)
	<?php $ctr = 1; $total = $product->childs->count(); ?>
	@foreach($product->childs as $child)
	<tr class="row-child">
		<td>
			<span class="font-bold">&nbsp;&nbsp;&nbsp;<i class="glyph-icon icon-angle-double-right"></i>&nbsp;&nbsp;&nbsp;{{ $child->title }}</span>
		</td>
		@if($selectedPriceList != null)
		<?php 
		$priceBase = $child->getPriceList($selectedPriceList->name);
		$price = $child->getPriceList($selectedPriceList->name, $member);
		if($price != null){
			$basePrice = ($priceBase != null) ? $priceBase->is_tba ? 'TBA' : $priceBase->base_price : 'n/a';
			$memberPrice = '';
			if($price->member_price != null){
				$memberPrice = ($price->is_tba) ? 'TBA' : $price->member_price;
			} else {
				if($priceBase != false){
					$memberPrice = ($priceBase->is_tba) ? 'TBA' : $priceBase->base_price;
				}
			}
		} else {
			$basePrice = ($priceBase != null) ? $priceBase->is_tba ? 'TBA' : $priceBase->base_price : 'n/a';
			$memberPrice = ($priceBase != null) ? $priceBase->is_tba ? 'TBA' : $priceBase->base_price : 'n/a';
		}
		?>
		<td>{{ $basePrice }}</td>
		<td>
			<input type="hidden" name="{{ $product->name }}[{{ $ctr }}][{{ $child->name }}]" value="{{ $child->name }}">
			<input type="text" name="{{ $product->name }}[{{ $ctr }}][{{ $selectedPriceList->name }}]" class="form-control input-price {{ $selectedPriceList->name }}" placeholder="{{ $selectedPriceList->title }} Price" data-price-name="{{ $selectedPriceList->name }}" value="{{ $memberPrice }}">
		</td>
		@else
		<td colspan="2">n/a</span>
			@endif
		</tr>
		<?php $ctr++; ?>
		@endforeach
		@endif
		@empty
		@endforelse
	</tbody>
</table>