<div class="panel">
    <div class="panel-body">
        <ul class="list-group row">
            <li class="col-md-3">
                <a href="{{ route('member.edit', $member->id) }}" class="list-group-item {{ Request::is('admin/member/*/edit') ? 'active' : '' }}">
                    <i class="glyph-icon icon-dashboard"></i>
                    Profile
                    <i class="glyph-icon icon-chevron-right"></i>
                </a>
            </li>
            <li class="col-md-3">
                <a href="{{ route('member.edit.price-lists', $member->id) }}" class="list-group-item {{ Request::is('admin/member/*/price-list') ? 'active' : '' }}">
                    <i class="glyph-icon font-red icon-money"></i>
                    Price Lists
                    <i class="glyph-icon font-blue-alt icon-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>