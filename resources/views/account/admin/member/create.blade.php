@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li><a href="{{ route('members') }}">Members</a></li>
<li class="active">Create Member</li>
@stop

@section('page-heading', 'Create Member')

@section('main')
<div class="panel">
	<div class="panel-body">
		@if($authUser->hasPermission('create_member'))
		{!! Form::open(['route' => 'member.store', 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
		<div class="form-group">
            <label class="col-sm-3 control-label">First Name <span class="req">*</span></label>
            <div class="col-sm-6">
                <input type="text" name="firstname" class="form-control {{ ($errors->has('firstname')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ old('firstname') }}">
                @if ($errors->has('firstname'))
                <label class="font-red font-size-11">{{ $errors->first('firstname') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Last Name</label>
            <div class="col-sm-6">
                <input type="text" name="lastname" class="form-control {{ ($errors->has('lastname')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ old('lastname') }}">
                @if ($errors->has('lastname'))
                <label class="font-red font-size-11">{{ $errors->first('lastname') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Address</label>
            <div class="col-sm-6">
                <input type="text" name="address" class="form-control {{ ($errors->has('address')) ? 'parsley-error' : '' }}" placeholder="Address" value="{{ old('address') }}">
                @if ($errors->has('address'))
                <label class="font-red font-size-11">{{ $errors->first('address') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Contact Number</label>
            <div class="col-sm-6">
                <input type="text" name="contact_number" class="form-control {{ ($errors->has('contact_number')) ? 'parsley-error' : '' }}" placeholder="Contact Number" value="{{ old('contact_number') }}">
                @if ($errors->has('contact_number'))
                <label class="font-red font-size-11">{{ $errors->first('contact_number') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
            <div class="col-sm-6">
                <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                <label class="font-red font-size-11">{{ $errors->first('email') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Username <span class="req">*</span></label>
            <div class="col-sm-6">
                <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ old('username') }}">
                @if ($errors->has('username'))
                <label class="font-red font-size-11">{{ $errors->first('username') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Password <span class="req">*</span></label>
            <div class="col-sm-6">
                <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password" value="{{ old('password') }}">
                @if ($errors->has('password'))
                <label class="font-red font-size-11">{{ $errors->first('password') }}</label>
                @endif
            </div>
        </div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Password Confirmation <span class="req">*</span></label>
		    <div class="col-sm-6">
		        <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
		        @if ($errors->has('password_confirmation'))
		        <label class="font-red font-size-11">{{ $errors->first('password_confirmation') }}</label>
		        @endif
		    </div>
		</div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Price List <span class="req">*</span></label>
            <div class="col-sm-6">
                <select name="price_list_id" class="custom-select">
                @foreach($priceLists as $list)
                <option value="{{ $list->id }}">{{ $list->title }}</option>
                @endforeach
                </select>
                @if ($errors->has('price_list_id'))
                <label class="font-red font-size-11">{{ $errors->first('price_list_id') }}</label>
                @endif
            </div>
        </div>
		<div class="form-group">
            <label class="col-sm-3 control-label">&nbsp;</label>
            <div class="col-sm-6 text-right">
                <a href="{{ route('members') }}" class="btn btn-sm btn-danger">CANCEL</a>
                <button type="submit" class="btn btn-success btn-sm">CREATE</button>
            </div>
        </div>
        {!! Form::close() !!}
		@else
		<h5>Lack of permission to create a new member.</h5>
		@endif
	</div>
</div>
@stop