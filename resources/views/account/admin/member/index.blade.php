@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Members</li>
@stop

@section('page-heading', 'Members')

@section('main')
<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero clearfix">List of Created Members @if($authUser->hasPermission('create_admins'))<a href="{{ route('member.create') }}" class="btn btn-blue-alt mrg10L">CREATE NEW</a>@endif</h3>
		@if($authUser->hasPermission('view_members'))
		<table class="table table-hover">
			<thead>
				<tr>
					<th width="20%">First Name</th><th width="20%">Username</th><th width="25%">Email</th><th width="30%" class="text-right">Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($members as $member)
				<tr>
					<td>{{ $member->firstname }}</td>
					<td>{{ $member->username }}</td>
					<td>{{ $member->email }}</td>
					<td class="text-right">
						@if($authUser->hasPermission('update_member'))
						<a href="{{ route('member.edit', $member->id) }}" class="btn btn-xs btn-success">EDIT</a>
						@endif
						@if($authUser->hasPermission('delete_member'))
						<a href="{{ route('member.delete', $member->id) }}" class="btn btn-xs btn-danger">DELETE</a>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="4">No members found!</td>
				</tr>
				@endforelse
			</tbody>
		</table>	
		@else
		<h5>Lack of permission to view the list of created members.</h5>
		@endif
	</div>
</div>
@stop