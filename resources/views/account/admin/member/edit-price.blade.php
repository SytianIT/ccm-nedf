@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li><a href="{{ route('members') }}">Members</a></li>
<li class="active">Edit Member Price Lists</li>
@stop

@section('page-heading', 'Edit "'.$member->firstname.'" Price Lists')

@section('main')
@include('account.admin.member.inc.sub-menu')
<div class="panel">
    <div class="panel-body">
        <div class="example-box-wrapper">
            @if($authUser->hasPermission('update_member'))
            @if($member->priceList == null)
            <div class="alert alert-warning mrg10B">
			    <div class="bg-orange alert-icon">
			        <i class="glyph-icon icon-warning"></i>
			    </div>
			    <div class="alert-content">
			    <h4 class="alert-title">Warning</h4>
			        <p>No current price list assigned to this user.</p>
			    </div>
			</div>
			@endif
            {!! Form::model($member, ['route' => ['member.update.price-lists', $member->id], 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">Price Lists Assigned</label>
                <div class="col-sm-6">
                    <select name="price_list_id" class="price-allocation custom-select" autocomplete="off">
	                @foreach($priceLists as $list)
	                <option value="{{ $list->id }}" {{ $member->price_list_id == $list->id ? 'selected' : '' }}>{{ $list->title }}</option>
	                @endforeach
	                </select>
	                <div class="text-center mrg20T">
	                	<div id="table-preloading" class="glyph-icon icon-spin-4 icon-spin font-size-28 hidden"></div>
	                </div>
	                @if($errors->has('price_list_id'))
	                <label class="font-red font-size-11">{{ $errors->first('price_list_id') }}</label>
	                @endif
                </div>
            </div>
            <div id="price-lists-container" class="form-group">
            	<div id="price-lists-table" class="col-sm-6 col-sm-offset-3">
	            	<table class="table table-bordered table-hover table-pricelists">
						<thead>
							<tr>
								<th width="40%">&nbsp;</th>
								<th width="60%" colspan="2" class="text-center">{{ $member->priceList != null ? $member->priceList->title : 'n/a' }}</th>
							</tr>
							<tr class="row-heading">
								<th>Product</th>
								<th width="30%" class="text-center text-uppercase">Base Price</th>
								<th width="30%" class="text-center text-uppercase">Member Price</th>
							</tr>
						</thead>
						<tbody>
							@forelse($products as $product)
							<tr class="row-parent">
								<td colspan="6" width="15%">{{ $product->title }}</td>
							</tr>
							@if($product->childs->count() > 0)
								<?php $ctr = 1; $total = $product->childs->count(); ?>
								@foreach($product->childs as $child)
								<tr class="row-child">
									<td>
										<span class="font-bold">&nbsp;&nbsp;&nbsp;<i class="glyph-icon icon-angle-double-right"></i>&nbsp;&nbsp;&nbsp;{{ $child->title }}</span>
									</td>
									@if($member->priceList != null)
									<?php 
										$priceBase = $child->getPriceList($member->priceList->name);
										$price = $child->getPriceList($member->priceList->name, $member);
										if($price != null){
											$basePrice = ($priceBase != null) ? $priceBase->is_tba ? 'TBA' : $priceBase->base_price : 'n/a';
											$memberPrice = '';
											if($price->member_price != null){
												$memberPrice = ($price->is_tba) ? 'TBA' : $price->member_price;
											} else {
												if($priceBase != false){
													$memberPrice = ($priceBase->is_tba) ? 'TBA' : $priceBase->base_price;
												}
											}
										} else {
											$basePrice = ($priceBase != null) ? $priceBase->is_tba ? 'TBA' : $priceBase->base_price : 'n/a';
											$memberPrice = ($priceBase != null) ? $priceBase->is_tba ? 'TBA' : $priceBase->base_price : '';
										}
									?>
									<td>{{ $basePrice }}</td>
									<td>
										<input type="hidden" name="{{ $product->name }}[{{ $ctr }}][{{ $child->name }}]" value="{{ $child->name }}">
										<input type="text" name="{{ $product->name }}[{{ $ctr }}][{{ $member->priceList->name }}]" class="{{ ($product->name == 'welded') ? 'with-less' : '' }} form-control input-price {{ $member->priceList->name }}" placeholder="{{ $member->priceList->title }} Price" data-price-name="{{ $member->priceList->name }}" value="{{ $memberPrice }}">
										@if($product->name == 'welded')
										<em class="font-size-11 font-red">( less .6% )</em>
										@endif
									</td>
									@else
									<td colspan="2">n/a</span>
									@endif
								</tr>
								<?php $ctr++; ?>
								@endforeach
							@endif
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <a href="{{ route('member.edit', $member->id) }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">UPDATE</button>
                </div>
            </div>
            {!! Form::close() !!}
            @else
            <h5>Lack of permission to make changes for this member price list.</h5>
            @endif
        </div>
    </label>
</div>
@stop

@section('scripts')
<script>
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('.input-price.with-less').on('change', function(){
		var $this = $(this),
			currVal = $this.val(),
			percentVal = 0.006 * parseInt(currVal),
			lessVal = currVal - percentVal;

		if($.isNumeric(currVal)){
			if($.isNumeric(percentVal)){
				if(percentVal > currVal){
					$this.val(0);
				} else {
					$this.val(lessVal);
				}
			} else {
				$this.val(currVal);
			}
		} else {
			$this.val(currVal);
		}
	});

	$('.price-allocation').on('change', function(){
		var value = $(this).find('option:selected').val();

		$.ajax({
			url : '{{ route('member.get.price-lists', $member->id) }}',
			type : 'POST',
			dataType : 'html',
			data : {price_list_key : value},
			beforeSend : function(){
				$('#price-lists-table table').remove();
				$('#table-preloading').removeClass('hidden');
			},
			error : function(response){
				$('#table-preloading').addClass('hidden');
				if($('#price-lists-table h5').length < 1){
					$('#price-lists-table').append('<h5>Error processing the request, please try again later.</h5>');
				}
			},
			success : function(response){
				$('#table-preloading').addClass('hidden');
				$('#price-lists-table').html(response);
			}
		});
	});
</script>
@stop