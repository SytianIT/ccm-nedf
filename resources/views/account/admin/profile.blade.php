@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Profile</li>
@stop

@section('page-heading', 'Profile')

@section('main')
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			{!! Form::model($authUser, ['route' => ['admin.profile.update', $authUser->id], 'method' => 'POST', 'class' => 'form-horizontal bordered-row', 'enctype' => 'multipart/form-data']) !!}
			<div class="form-group">
                <label class="col-sm-3 control-label">Profile Image</label>
                <div class="col-sm-6">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="max-width: 150px; max-height: 150px;"><img src="{{ $authUser->profile_img != null ? asset($authUser->profile_img) : '' }}" class="img-responsive"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <input name="image" type="file" id="file-browse">
                            <div class="ripple-wrapper"></div></span>
                            <em class="font-size-11">For better results, please provide an image with a resolution of 150 x 150.</em>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-3 control-label">First Name <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="firstname" class="form-control {{ ($errors->has('firstname')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ $authUser->firstname ?: old('firstname') }}">
                    @if ($errors->has('firstname'))
                    <label class="font-red font-size-11">{{ $errors->first('firstname') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-6">
                    <input type="text" name="lastname" class="form-control {{ ($errors->has('lastname')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ $authUser->lastname ?: old('lastname') }}">
                    @if ($errors->has('lastname'))
                    <label class="font-red font-size-11">{{ $errors->first('lastname') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Contact Number</label>
                <div class="col-sm-6">
                    <input type="text" name="contact_number" class="form-control {{ ($errors->has('contact_number')) ? 'parsley-error' : '' }}" placeholder="Contact Number" value="{{ $authUser->contact_number ?: old('contact_number') }}">
                    @if ($errors->has('contact_number'))
                    <label class="font-red font-size-11">{{ $errors->first('contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ $authUser->email ?: old('email') }}">
                    @if ($errors->has('email'))
                    <label class="font-red font-size-11">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Username <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ $authUser->username ?: old('username') }}">
                    @if ($errors->has('username'))
                    <label class="font-red font-size-11">{{ $errors->first('username') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Password <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password" value="">
                    <p><em class="font-size-11">(To change password type in here the new password)</em></p>
                    @if ($errors->has('password'))
                    <label class="font-red font-size-11">{{ $errors->first('password') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Password Confirmation <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
                    @if ($errors->has('password_confirmation'))
                    <label class="font-red font-size-11">{{ $errors->first('password_confirmation') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('users') }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">UPDATE</button>
                </div>
            </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		$('.fileinput-new').on('click', function(){
			$('#file-browse').click();
		});

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.fileinput-preview').find('img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$('#file-browse').on('change', function(){
			readURL(this);
		});
	});
</script>
@stop