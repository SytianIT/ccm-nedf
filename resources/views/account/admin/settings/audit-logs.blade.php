@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Audit Logs</li>
@stop

@section('page-heading', 'Audit Logs')

@section('main')
<div class="panel">
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Description</th>
					<th width="15%">Timestamp</th>
				</tr>
			</thead>
			<tbody>
				@forelse($logs as $log)
				<tr>
					<td>{{ $log->description }} - <strong>by: {{ $log->user->username }}</strong></td>
					<td>{{ $log->created_at->toDayDateTimeString() }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="2">No logs found!</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		<div class="text-center">
			{{ $logs->render() }}
		</div>
	</div>
</div>
@stop