@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Site Settings</li>
@stop

@section('page-heading', 'Site Settings')

@section('main')
<div class="panel">
	<div class="panel-body">
		@if($authUser->hasPermission('access_site_settings'))
		<div class="example-box-wrapper">
			{!! Form::model($contents, ['route' => ['admin.settings.update'], 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
			<div class="form-group">
                <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ $contents->email ?: old('email') }}">
                    @if ($errors->has('email'))
                    <label class="font-red font-size-11">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-3 control-label">Site Title <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="site_title" class="form-control {{ ($errors->has('site_title')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ $contents->site_title ?: old('site_title') }}">
                    @if ($errors->has('site_title'))
                    <label class="font-red font-size-11">{{ $errors->first('site_title') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Enable System</label>
                <div class="col-sm-6">
                   	<input type="checkbox" class="input-switch" name="enabled" {{ ($contents->is_active) ? 'checked' : '' }}>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('users') }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">UPDATE</button>
                </div>
            </div>
			{!! Form::close() !!}
		</div>
		@else
		<h5>Lack of permission to make changes in site settings</h5>
		@endif
	</div>
</div>
@stop