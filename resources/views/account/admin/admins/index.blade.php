@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li class="active">Admin Users</li>
@stop

@section('page-heading', 'Admin Users')

@section('main')
<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero clearfix">List of Created Users @if($authUser->hasPermission('create_admins'))<a href="{{ route('user.create') }}" class="btn btn-blue-alt mrg10L">CREATE NEW</a>@endif</h3>
		@if($authUser->hasPermission('view_admins'))
		<table class="table table-hover">
			<thead>
				<tr>
					<th width="20%">First Name</th><th width="20%">Username</th><th width="25%">Email</th><th width="30%" class="text-right">Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($users as $user)
				<tr>
					<td>{{ $user->firstname }}</td>
					<td>{{ $user->username }}</td>
					<td>{{ $user->email }}</td>
					<td class="text-right">
						@if($authUser->hasPermission('update_admins'))
						<a href="{{ route('user.edit', $user->id) }}" class="btn btn-xs btn-success">EDIT</a>
						@endif
						@if($authUser->hasPermission('delete_admins'))
						<a href="{{ route('user.delete', $user->id) }}" class="btn btn-xs btn-danger">DELETE</a>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="4">No admin users found!</td>
				</tr>
				@endforelse
			</tbody>
		</table>	
		@else
		<h5>Lack of permission to view the list of create users.</h5>
		@endif
	</div>
</div>
@stop