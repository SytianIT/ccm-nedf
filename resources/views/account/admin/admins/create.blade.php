@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin') }}">Dashboard</a></li>
<li><a href="{{ route('users') }}">Admin Users</a></li>
<li class="active">Create Admin User</li>
@stop

@section('page-heading', 'Create Admin User')

@section('main')
@if($roles->count() < 1)
<div class="alert alert-warning mrg10B">
    <div class="bg-orange alert-icon">
        <i class="glyph-icon icon-warning"></i>
    </div>
    <div class="alert-content">
    <h4 class="alert-title">Warning</h4>
        <p>There are 0 roles found, creation of user may not proceed. <a href="{{ route('role.create') }}" title="Link">Create Role Now!</a></p>
    </div>
</div>
@endif
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			@if($authUser->hasPermission('create_admins'))
			{!! Form::open(['route' => 'user.store', 'method' => 'POST', 'class' => 'form-horizontal bordered-row']) !!}
			<div class="form-group">
                <label class="col-sm-3 control-label">First Name <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="firstname" class="form-control {{ ($errors->has('firstname')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ old('firstname') }}">
                    @if ($errors->has('firstname'))
                    <label class="font-red font-size-11">{{ $errors->first('firstname') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-6">
                    <input type="text" name="lastname" class="form-control {{ ($errors->has('lastname')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ old('lastname') }}">
                    @if ($errors->has('lastname'))
                    <label class="font-red font-size-11">{{ $errors->first('lastname') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Contact Number</label>
                <div class="col-sm-6">
                    <input type="text" name="contact_number" class="form-control {{ ($errors->has('contact_number')) ? 'parsley-error' : '' }}" placeholder="Contact Number" value="{{ old('contact_number') }}">
                    @if ($errors->has('contact_number'))
                    <label class="font-red font-size-11">{{ $errors->first('contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Email <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <label class="font-red font-size-11">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Username <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ old('username') }}">
                    @if ($errors->has('username'))
                    <label class="font-red font-size-11">{{ $errors->first('username') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Password <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password" value="{{ old('password') }}">
                    @if ($errors->has('password'))
                    <label class="font-red font-size-11">{{ $errors->first('password') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Password Confirmation <span class="req">*</span></label>
                <div class="col-sm-6">
                    <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
                    @if ($errors->has('password_confirmation'))
                    <label class="font-red font-size-11">{{ $errors->first('password_confirmation') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Role <span class="req">*</span></label>
                <div class="col-sm-6">
                    <select name="role_id[]" id="role_id" multiple class="roles multi-select {{ ($errors->has('role_id')) ? 'parsley-error' : '' }}">
                        @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('role_id'))
                    <label class="font-red font-size-11">{{ $errors->first('role_id') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('users') }}" class="btn btn-sm btn-danger">CANCEL</a>
                    <button type="submit" class="btn btn-success btn-sm">SAVE</button>
                </div>
            </div>
			{!! Form::close() !!}
			@else
			<h5>Lack of permission to create new role.</h5>
			@endif
		</div>
	</div>
</div>
@stop