$(document).ready(function(){

	/**
	 * Window Ready Functions
	 */
    
    /**
     * Script to initialize Switch element
     */
    $(".confirm-submit").click(function(e) {
        var msg = $(this).data('confirm') || 'Are you sure?';
        var msgText = $(this).data('confirm-text') || '';
        var form  = $(this).data('form');
        e.preventDefault();
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#4456BA",
        }).then(function() {
            $(form).submit();
            swal({
                title: '<i>Loading...</u>',
                type: 'info',
                showCloseButton: false,
                showCancelButton: false,
                showConfirmButton: false,
                html:
                'Please wait as we process your request. <div class="text-center"><div class="glyph-icon icon-spin-4 icon-spin font-size-28" style="display:inline-block"></div></div>',
            });
        });
    });

    $(".confirm-alert").click(function(e) {
        var msg = $(this).data('confirm') || 'Are you sure?';
        var msgText = $(this).data('confirm-text') || '';
        e.preventDefault();
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false,
        }).then(function() {
            window.location = _self.attr("href")
        });
    });
	
    /**
     * Script to initialize Switch element
     */
    $('.input-switch').bootstrapSwitch();

     /**
     * Script to initialize Input Mask
     */
    $(".input-mask").inputmask();

	/**
     * Script to initilize error messages at top of screen
     * @type {[type]}
     */
    var notifBar = $('#notification-show');
    if(notifBar.length > 0){
        var notes = [];

        // notes[''] = '<i class="glyph-icon icon-cog mrg5R"></i>This is a default notification message.';
        // notes['alert'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an alert notification message.';
        // notes['error'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an error notification message.';
        // notes['success'] = '<i class="glyph-icon icon-cog mrg5R"></i>You successfully read this important message.';
        // notes['information'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is an information notification message!';
        // notes['notification'] = '<i class="glyph-icon icon-cog mrg5R"></i>This alert needs your attention, but it\'s for demonstration purposes only.';
        // notes['warning'] = '<i class="glyph-icon icon-cog mrg5R"></i>This is a warning for demonstration purposes.';
        // 
        var self = notifBar;
        noty({
            text: self.data('message'),
            type: self.data('type'),
            dismissQueue: true,
            theme: 'agileUI',
            layout: self.data('layout')
        });
    }

    /* Multiselect inputs */
    $(function() { "use strict";
        $(".spinner-input").spinner();

        $('.input-switch').bootstrapSwitch();

        $(".multi-select").multiSelect();
        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    });
    
	/**
	 * Window Load Functions
	 */
	$(window).load(function(){
		$(window).resize();

		/**
		 * Function to call when page loaded hide the preloader
		 * @param  {function} )   { $('#loading').fadeOut( 400, "linear" ); } 
		 * @param  {Count} 300 
		 * @return {Hide Elemenet}     
		 */
		setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
	});

	/**
	 * Window Resize Function
	 */
	$('window').on('resize', function(){

	});
	$(window).resize();
});